from threading import Thread

class TapMeasurement():
    '''
    Base class for tap measurement context.
    Test cases make tap measurements based on methods defined here.
    '''

    def __init__(self, indicators, point):
        self.indicators = indicators
        self.point = point
        self.results = None
        self.timeout = 1.0
        self.thread = Thread(target=self._thread_main)

    def start(self, timeout=6.0):
        # Initialize members used in tap measurement.
        self.timeout = timeout
        self.results = []

        self._start()

        self.thread.start()

    def end(self):
        self.thread.join()

    def _start(self):
        pass

    def _thread_main(self):
        self._read_results()
        self._update_tap_coordinate_indicators()

    def _read_results(self):
        pass

    def _update_tap_coordinate_indicators(self):
        if self.results != []:
            self.indicators.X = self.results[0][0]
            self.indicators.Y = self.results[0][1]
        else:
            self.indicators.X = '-'
            self.indicators.Y = '-'

class ContinuousMeasurement():
    '''
    Base class for continuous measurement (e.g. swipe) context.
    Test cases make continuous measurements based on methods defined here.
    '''

    def __init__(self, indicators, line):
        self.indicators = indicators
        self.line = line
        self.timeout = 0.5
        self.start_timeout = 6.0
        self.results = None
        self.thread = Thread(target=self._thread_main)

    def start(self, timeout=0.5, start_timeout=6.0):
        '''
        Start continuous measurement in a separate thread.
        :param timeout: Timeout in seconds between consecutive measurements.
        :param start_timeout: Timeout in seconds for the first measurement.
        '''

        self.timeout = timeout
        self.start_timeout = start_timeout
        self.results = []

        self._start()

        self.thread.start()

    def end(self):
        self.thread.join()

        # Show last measured point in UI.
        self._update_line_coordinate_indicators()

    def _start(self):
        pass

    def _thread_main(self):
        self._read_results()
        self._update_line_coordinate_indicators()

    def _read_results(self):
        pass

    def _update_line_coordinate_indicators(self):
        if self.results != []:
            self.indicators.X = self.results[-1][0][0]
            self.indicators.Y = self.results[-1][0][1]
        else:
            self.indicators.X = '-'
            self.indicators.Y = '-'

    def parse_data(self):
        results = self.results
        touchlist = []
        #last_coord = []
        if len(results) > 0:
            for cline_data in results:
                if "OK" in cline_data:
                    for point in range(cline_data.index("OK")):  # Read all data points before "OK"
                        if len(cline_data[point]) > 9:
                            touchlist.append([cline_data[point][0], cline_data[point][1],
                                              cline_data[point][2], cline_data[point][3],
                                              cline_data[point][4], cline_data[point][5],
                                              cline_data[point][6], cline_data[point][7],
                                              cline_data[point][8], cline_data[point][9]])
                        else:
                            touchlist.append([cline_data[point][0], cline_data[point][1],
                                              cline_data[point][2], cline_data[point][3],
                                              cline_data[point][4], cline_data[point][5],
                                              cline_data[point][6]])
        return touchlist

    def save_data_to_csv_file(self, line_id, file_path, write_header=False, replace_file=False):
        touchlist = self.parse_data()

        write_parameter = 'a'
        if replace_file:
            write_parameter = 'w'
        with open(file_path, write_parameter) as file:
            if write_header:
                Header = "Panel X,Panel Y,Panel Z,Finger ID,Timestamp_EXT,Timestamp_INT,Event,Line ID\n"
                file.write(Header)
            for testresult in touchlist:
                resultstring = str(testresult).strip("[]")
                file.write(resultstring + "," +str(line_id) +"\n")
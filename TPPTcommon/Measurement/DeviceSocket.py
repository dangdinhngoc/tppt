from .Base import *
import queue

class TapMeasurementDeviceSocket(TapMeasurement):
    '''
    Tap measurement from device socket (TCP).
    '''

    def __init__(self, indicators, point, dsock):
        super(TapMeasurementDeviceSocket, self).__init__(indicators, point)

        self.dsock = dsock

    def _start(self):
        # Clear queue before launching the measurement thread.
        self.dsock.clear_queue()

    def _read_results(self):
        # Wait for the first event from the device and store that as result. Returns empty list if timeout occurs.
        self.results = self.dsock.ReturnP2PArray(timeout=self.timeout)

class ContinuousMeasurementDeviceSocket(ContinuousMeasurement):
    '''
    Continuous measurement from device socket (TCP).
    '''

    def __init__(self, indicators, line, dsock):
        super(ContinuousMeasurementDeviceSocket, self).__init__(indicators, line)

        self.dsock = dsock

    def _start(self):
        # Clear queue before launching the measurement thread.
        self.dsock.clear_queue()

    def _read_results(self):
        try:
            # Read first event with timeout that should be sufficiently long for effector to reach DUT surface.
            item = self.dsock.queue.get(timeout=self.start_timeout)
            self.dsock.queue.task_done()

            if item is None:
                return

            self.results.append(item)

            # Read events until timeout is exceeded
            for touch in self.dsock.CLine(timeout=self.timeout):
                self.results.append(touch)
        except queue.Empty:
            pass # There was no event reported within timeout. This ends the measurement.
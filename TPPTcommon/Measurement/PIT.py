from .Base import *

class TapMeasurementPIT(TapMeasurement):
    '''
    Tap measurement from PIT box.
    Todo: Needs to be tested.
    '''

    def __init__(self, indicators, point, pit, dut_index):
        super(TapMeasurementPIT, self).__init__(indicators, point)

        self.pit = pit
        self.dut_index = dut_index

    def _start(self):
        pass

    def _read_results(self):
        try:
            self.results = self.pit.ReturnP2PArray(self.dut_index)
        except:
            pass # Todo: Which exception is this supposed to handle? Should only pass "no event" type of errors.

class ContinuousMeasurementPIT(ContinuousMeasurement):
    '''
    Continuous measurement from PIT box.
    Todo: Needs to be tested.
    '''

    def __init__(self, line, indicators, pit):
        super(ContinuousMeasurementPIT, self).__init__(indicators, line)

        self.pit = pit

    def _start(self):
        pass

    def _read_results(self):
        for touch in self.pit.CLine():
            self.results.append(touch)
from .Node import *

class SettingsNode(Node):
    """
    Node that stores general settings for running test cases.
    """

    def __init__(self):
        super().__init__('Settings')

        self.controls.line_drawing_speed = 30
        self.controls.finger_depth = -0.5
        self.controls.grid_spacing = 5.0
        self.controls.default_speed = 50
        self.controls.default_acceleration = 50
        self.controls.default_margin = 4
        self.controls.default_duration = 0.15
        self.controls.loop = 3


        self.controls.info['line_drawing_speed'] = {'label': 'Line drawing speed [mm/s]', 'min': 1}
        self.controls.info['grid_spacing'] = {'label': 'Grid spacing [mm]', 'min': 1}
        self.controls.info['finger_depth'] = {'label': 'finger_depth [mm]', 'min': -1}
        self.controls.info['default_speed'] = {'label': 'Robot speed [mm/s]', 'min': 1}
        self.controls.info['default_acceleration'] = {'label': 'Acceleration [mm/s^2]', 'min': 1}
        self.controls.info['default_margin'] = {'label': 'Margin/Boundary[mm]','min':0.0}
        self.controls.info['default_duration'] = {'label':'Tap Duration'}
        self.controls.info['loop'] = {'label': 'Loop','min':1}
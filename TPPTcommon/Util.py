def calculate_timeout(speed):
    data_capturing_timeout = 3.0

    if speed < 70:
        data_capturing_timeout += 3.0
    if speed < 30:
        data_capturing_timeout += 3.0

    return data_capturing_timeout
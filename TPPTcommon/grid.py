import collections
import matplotlib.pyplot as plt
import matplotlib.patches as pltpatches
import numpy
import math
import csv
import mimetypes
import cv2
import base64
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

import TPPTcommon.containers as Containers

def line_coord_of_closest_rectangle_intersection(min_x, max_x, start_x, dir_x):
    """
    Get closest parametric line coordinate of intersection point of the line and
    rectangle along one dimension.
    :param min_x: Rectangle minimum along selected dimension.
    :param max_x: Rectangle maximum along selected dimension.
    :param start_x: Line start point coordinate in selected dimension.
    :param dir_x: Line direction vector coordinate in selected dimension.
    :return: Line parameter at closest intersection.
    """

    # Direction vector determines which of the two possible rectangle edges is hit first.
    if dir_x > 0:
        return (min_x - start_x) / dir_x
    elif dir_x < 0:
        return (max_x - start_x) / dir_x

    # Direction is perpendicular to selected dimension. Return -1 so these planes are not hit.
    return -1


def clip_line_start_to_rectangle(line, width, height, border_width):
    """
    Clip line start to axis aligned rectangle center area (determined by border_width).
    If line is completely outside of the rectangle, the line is collapsed into a point.
    :param line: A line with start and end points.
    :param width: Width of the rectangle.
    :param height: Height of the rectangle.
    :param border_width: Border width of the rectangle.
    """

    # Line direction vector components.
    dx = line.end_x - line.start_x
    dy = line.end_y - line.start_y

    # Clip region min and max coordinates.
    min_x = border_width
    max_x = width - border_width
    min_y = border_width
    max_y = height - border_width

    # Determine line parameters for two potential closest hit points.
    tx = line_coord_of_closest_rectangle_intersection(min_x, max_x, line.start_x, dx)
    ty = line_coord_of_closest_rectangle_intersection(min_y, max_y, line.start_y, dy)

    # Line start is inside the rectangle
    if tx < 0 and ty < 0:
        return

    # Determine which of the two coordinates corresponds to closest hit point.
    if tx > ty:
        t = tx
        px = line.start_x + dx * t
        py = line.start_y + dy * t

        # Check if the hit point is within the region boundary.
        if min_y <= py <= max_y:
            line.start_x = px
            line.start_y = py
        else:
            line.start_x = line.end_x
            line.start_y = line.end_y
    else:
        t = ty
        px = line.start_x + dx * t
        py = line.start_y + dy * t

        # Check if the hit point is within the region boundary.
        if min_x <= px <= max_x:
            line.start_x = px
            line.start_y = py
        else:
            line.start_x = line.end_x
            line.start_y = line.end_y


def clip_line_to_rectangle(line, width, height, border_width):
    """
    Clip line to axis aligned rectangle center area (determined by border_width).
    :param line: A line with start and end points.
    :param width: Width of the rectangle.
    :param height: Height of the rectangle.
    :param border_width: Border width of the rectangle.
    :return:
    """

    # Clip line start point to region.
    clip_line_start_to_rectangle(line, width, height, border_width)

    # Swap line start and end points and again clip line start point to region. Then swap start and end points back.
    line.start_x, line.start_y, line.end_x, line.end_y = line.end_x, line.end_y, line.start_x, line.start_y
    clip_line_start_to_rectangle(line, width, height, border_width)
    line.start_x, line.start_y, line.end_x, line.end_y = line.end_x, line.end_y, line.start_x, line.start_y


def azimuth_direction(azimuth_angle):
    '''
    Calculates unit vector for given azimuth angle.
    ----------------------------------------
    Why is this needed:

    In our robots, we use left hand convention for x-y-z-axis and right hand convention for azimuth angles.
    With other words, the z-axis points up from DUT, x-axis from top left corner to top right corner, and
    y-axis from top left corner to bottom left corner. However, the angle grows anti-clockwise.
    ----------------------------------------
    :param azimuth_angle: the angle for which we want the unit vector (radians)
    :return: unit vector as a list [x,y] for given azimuth angle
    '''

    x_comp = numpy.cos(-azimuth_angle)
    y_comp = numpy.sin(-azimuth_angle)

    return [x_comp, y_comp]


class GridVisContainer:
    """
    Container for grid visualization. Contains all elements needed to visualize a test grid.
    Args:
    name: string
    panel_size: (width, height)
    items: List of objects derived from containers.TestAction to be plotted
    title: string [sub-plot title; e.g. DUT name]
    """
    def __init__(self, name, panel_size, items, title=None, projection_3d=False):
        self.name = name
        self.panel_size = panel_size
        self.items = items
        self.title = title
        self.projection_3d = projection_3d

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, hex(id(self)))


class GridVisualizer:
    """
    Class that is used to generate measurement point figures
    Usage:
        gv = GridVisualizer()
        gv.AddGridList(gridlist) # gv.AddGrid(grid) See AddGrid() or AddGridList()
        images = gv.Render()
    """
    def __init__(self):
        self.figure = Figure()
        self.grids = {}

    def Render(self):
        """
        Main rendering function, generates test grid images
        :return: Dictionary { 'test case name' : [list of plotted images] }
        """
        images = {}

        fig = self.figure
        fig_canvas = FigureCanvas(fig)

        for gesture in self.grids:
            grids = self.grids[gesture]

            if not isinstance(grids, collections.Iterable):
                grids = [grids]

            for grid_n in range(len(grids)):
                ax_n = 111
                active_grid = grids[grid_n]
                if active_grid.projection_3d: ax = fig.add_subplot(ax_n, title=active_grid.title, projection='3d')
                else: ax = fig.add_subplot(ax_n, title=active_grid.title)
                points = [[], [], [], []]

                normals = []
                for i in active_grid.items:
                    if isinstance(i, Containers.Point):
                        multi = False
                        try: multi = i.multifinger
                        except: pass
                        if multi:
                            multi_finger_points = self._CreateMultifingerTapPoints(i, i.angle)
                            for multi_finger_point in multi_finger_points:
                                points[0].append(multi_finger_point[0])
                                points[1].append(multi_finger_point[1])
                                points[2].append(multi_finger_point[2])
                                points[3].append(multi_finger_point[3])
                        else:
                            points[0].append(i.x)
                            points[1].append(i.y)
                            points[2].append(i.z)
                            if isinstance(i, Containers.TouchAreaPoint) and i.touch_area == 'edge_area':
                                points[3].append('r')
                            else:
                                points[3].append('b')


                    elif isinstance(i, Containers.Line):
                        multi = False
                        try: multi = i.multifinger
                        except: pass
                        if not multi:
                            color = "blue"
                            try: color = i.color
                            except:pass
                            if not active_grid.projection_3d: ax.arrow(i.start_x, i.start_y, i.end_x - i.start_x, i.end_y - i.start_y, head_width=2, head_length=2, fc=color, ec=color)
                            else: ax.plot([i.start_x, i.end_x], [i.start_y, i.end_y], zs = [i.start_z, i.end_z], color=color)
                        else:
                            arrows = self._CreateMultifingerLineArrows(i)
                            for arrow in arrows:
                                ax.arrow(arrow[0],arrow[1],arrow[2],arrow[3], head_width=arrow[4], head_length=arrow[5], fc=arrow[6], ec=arrow[6])

                if active_grid.projection_3d:
                    ax.scatter(points[0], points[1], points[2], color=points[3])
                    for normal in normals:
                        ax.plot(normal[0], normal[1], normal[2], linewidth=2)

                    max_range = numpy.array([max(points[0]) - min(points[0]), max(points[1]) - min(points[1]), max(points[2]) - min(points[2])]).max() / 2.0
                    mid_x = (max(points[0]) + min(points[0])) * 0.5
                    mid_y = (max(points[1]) + min(points[1])) * 0.5
                    mid_z = (max(points[2]) + min(points[2])) * 0.5
                    ax.set_xlim(mid_x - max_range, mid_x + max_range)
                    ax.set_ylim(mid_y + max_range, mid_y - max_range)
                    ax.set_zlim(mid_z - max_range, mid_z + max_range)
                else:
                    ax.scatter(points[0], points[1], color=points[3])

                if not active_grid.projection_3d:
                    ax.add_patch(pltpatches.Rectangle([0,0], active_grid.panel_size[0], active_grid.panel_size[1], edgecolor='black', fill=False))
                    ax.axis([-10, active_grid.panel_size[0] + 10, active_grid.panel_size[1] + 10, -10])
                    ax.set_xlabel("Measurement area width " + str(numpy.around(active_grid.panel_size[0], 2)) + " [mm]")
                    ax.set_ylabel("Measurement area height " + str(numpy.around(active_grid.panel_size[1],2)) + " [mm]")
                    ax.set_aspect('equal')
                else:
                    pass

                if gesture not in images:
                    images[gesture] = []
                images[gesture].append(self._fig_to_canvas(fig_canvas))

                fig.clear()

        return images

    def autowrap_text(self, textobj, renderer):
        """Wraps the given matplotlib text object so that it exceed the boundaries
        of the axis it is plotted in."""
        import textwrap
        # Get the starting position of the text in pixels...
        x0, y0 = textobj.get_transform().transform(textobj.get_position())
        # Get the extents of the current axis in pixels...
        clip = textobj.get_axes().get_window_extent()
        # Set the text to rotate about the left edge (doesn't make sense otherwise)
        textobj.set_rotation_mode('anchor')

        # Get the amount of space in the direction of rotation to the left and
        # right of x0, y0 (left and right are relative to the rotation, as well)
        rotation = textobj.get_rotation()
        right_space = self.min_dist_inside((x0, y0), rotation, clip)
        left_space = self.min_dist_inside((x0, y0), rotation - 180, clip)

        # Use either the left or right distance depending on the horiz alignment.
        alignment = textobj.get_horizontalalignment()
        if alignment is 'left':
            new_width = right_space
        elif alignment is 'right':
            new_width = left_space
        else:
            new_width = 2 * min(left_space, right_space)

        # Estimate the width of the new size in characters...
        aspect_ratio = 0.5  # This varies with the font!!
        fontsize = textobj.get_size()
        pixels_per_char = aspect_ratio * renderer.points_to_pixels(fontsize)

        # If wrap_width is < 1, just make it 1 character
        wrap_width = max(1, new_width // pixels_per_char)
        try:
            wrapped_text = textwrap.fill(textobj.get_text(), wrap_width)
        except TypeError:
            # This appears to be a single word
            wrapped_text = textobj.get_text()
        textobj.set_text(wrapped_text)

    def min_dist_inside(self, point, rotation, box):
        """Gets the space in a given direction from "point" to the boundaries of
        "box" (where box is an object with x0, y0, x1, & y1 attributes, point is a
        tuple of x,y, and rotation is the angle in degrees)"""
        from math import sin, cos, radians
        x0, y0 = point
        rotation = radians(rotation)
        distances = []
        threshold = 0.0001
        if cos(rotation) > threshold:
            # Intersects the right axis
            distances.append((box.x1 - x0) / cos(rotation))
        if cos(rotation) < -threshold:
            # Intersects the left axis
            distances.append((box.x0 - x0) / cos(rotation))
        if sin(rotation) > threshold:
            # Intersects the top axis
            distances.append((box.y1 - y0) / sin(rotation))
        if sin(rotation) < -threshold:
            # Intersects the bottom axis
            distances.append((box.y0 - y0) / sin(rotation))
        return min(distances)

    def _fig_to_canvas(self, fig_canvas):
        fig_canvas.draw()

        width, height = self.figure.get_size_inches() * self.figure.get_dpi()
        width = int(width)
        height = int(height)

        frame = numpy.fromstring(fig_canvas.tostring_rgb(), dtype='uint8').reshape(height, width, 3)
        bd = GridVisualizer.ndarray_to_img_src(frame, ".png")
        return bd

    def ndarray_to_img_src(image: numpy.ndarray, type=".jpg"):
        """Convert image into img.src source string"""
        mimetype, encoding = mimetypes.guess_type("base" + type)
        mimetype = mimetype if mimetype is not None else 'image/jpeg'
        r, data = cv2.imencode(type, image)
        bd = base64.b64encode(data)
        bd = "data:" + mimetype + ";base64," + bd.decode()
        return bd

    def _CreateMultifingerLineArrows(self, line):
        colour_LUT = ['#0000FF',
                      '#00FF00',
                      '#FF00FF',
                      '#00FFFF',
                      '#FFFF00',
                      '#FFA600',
                      '#8CFF00',
                      '#CC00FF',
                      '#FF7AB6',
                      '#000000',
                      '#7D7D7D']
        retval = []
        start_x = line.start_x
        start_y = line.start_y
        end_x = line.end_x
        end_y = line.end_y
        retval.append((start_x, start_y, end_x - start_x, end_y - start_y, 2, 2, colour_LUT[0]))

        angle = line.angle
        unit_vec = azimuth_direction(numpy.radians(angle))
        for i in range(line.fingers - 1):
            x_t = (i + 1) * line.finger_distance * unit_vec[0]
            y_t = (i + 1) * line.finger_distance * unit_vec[1]
            retval.append((start_x + x_t, start_y + y_t, end_x - start_x, end_y - start_y, 2, 2, colour_LUT[i + 1]))

        return retval

    def _CreateMultifingerTapPoints(self, point, angle=0.0):
        colour_LUT = ['#0000FF',
                      '#00FF00',
                      '#FF00FF',
                      '#00FFFF',
                      '#FFFF00',
                      '#FFA600',
                      '#8CFF00',
                      '#CC00FF',
                      '#FF7AB6',
                      '#000000',
                      '#7D7D7D']
        retval = []
        retval.append((point.x, point.y, 0.0, colour_LUT[0]))

        unit_vec = azimuth_direction(numpy.radians(angle))

        for i in range(point.fingers - 1):
            x_t = (i + 1) * point.finger_distance * unit_vec[0]
            y_t = (i + 1) * point.finger_distance * unit_vec[1]
            retval.append((point.x + x_t, point.y + y_t, 0.0, colour_LUT[i + 1]))

        return retval

    def AddGrid(self, name, grid):
        """
        Adds a single grid for visualization:
        args:
        name: name of the group(Test name usually) to add the grid to.
        grid: GridVisContainer-object
        """
        if name in self.grids:self.grids[name].append(grid)
        else:self.grids[name] = [grid]

    def AddGridList(self, griddict):
        """
        Adds a dictionary of grids for visualization:
        args:
        griddict: {"name":[GridVisContainer,],}
        """
        self.grids = {**self.grids, **griddict}

    def ClearGrids(self):
        """
        Clears all grids.
        """
        self.grids = {}


def create_random_points(dut, num_points):
    retval = []

    w_ = dut.width
    h_ = dut.height

    for point in range(num_points):
        retval.append(Containers.Point(numpy.random.rand(1)[0] * w_, numpy.random.rand(1)[0] * h_, 0))

    return retval


def create_non_stationary_reporting_rate_lines(dut):
    retval = []
    w_ = dut.width
    h_ = dut.height

    retval.append(Containers.Line(0.0, 0.0, 0.0, w_, h_, 0.0))
    retval.append(Containers.Line(w_ / 2.0, 0.0, 0.0, w_ / 2, h_, 0.0))
    retval.append(Containers.Line(0.0, h_ / 2.0, 0, w_, h_ / 2.0, 0.0))

    return retval

# ngocdd3

def create_non_stationary_reporting_rate_lines_custom(dut):
    retval = []
    margin = 10
    z = -0.1
    w_ = dut.width - margin
    h_ = dut.height - margin

    retval.append(Containers.Line(margin, margin, z, w_, h_, z))
    # retval.append(Containers.Line(margin, h_, z, w_, margin, z))
    retval.append(Containers.Line(w_ / 2.0, margin, z, w_ / 2.0, h_, z))
    retval.append(Containers.Line(margin, h_ / 2.0, z, w_, h_ / 2.0, z))
    # retval.append(Containers.Line(margin, margin, z, margin, h_, z))
    # retval.append(Containers.Line(margin, h_, z, w_, h_, z))
    # retval.append(Containers.Line(w_ - z, h_, z, w_ - z, margin, z))
    # retval.append(Containers.Line(w_, margin, z, margin, margin, z))
    return retval
# ngocdd3

def create_vertical_horizontal_line_grid(dut, grid_size):
    retval = []
    z = 0.0

    w_ = dut.width
    h_ = dut.height

    w_points = int(numpy.round(w_ / grid_size))
    if w_points == 0: w_points = 1

    h_points = int(numpy.round(h_ / grid_size))
    if h_points == 0: h_points = 1

    w_real_step = (w_) / w_points
    h_real_step = (h_) / h_points

    # Horizontal lines
    for y_ in range(h_points + 1):
        retval.append(Containers.Line(0.0, y_ * h_real_step, z, w_, y_ * h_real_step, z))
    # Vertical lines
    for x_ in range(w_points + 1):
        retval.append(Containers.Line(x_ * w_real_step, 0.0, z, x_ * w_real_step, h_, z))

    return retval


def create_diagonal_line_grid(dut, dist):
    retval = []
    w_ = dut.width
    h_ = dut.height
    z = 0.0

    delta_x = dist / (math.sin(math.atan(h_ / w_)))
    delta_y = delta_x * (h_ / w_)

    # Horizontal lines
    start_point = [0.0, 0.0]
    end_point = [w_, h_]
    i = 1

    while True:
        if start_point[0] + delta_x * i > w_ or start_point[0] + delta_x * i < 0.0 or end_point[1] - delta_y * i < 0.0:
            break
        retval.append(Containers.Line(delta_x * i, 0.0, z, end_point[0], end_point[1] - delta_y * i, z))
        i += 1

    i = 0
    while True:
        if start_point[1] + delta_y * i > h_ or start_point[1] + delta_y * i < 0.0 or end_point[0] - delta_x * i < 0.0:
            break
        retval.append(Containers.Line(0.0, delta_y * i, z, end_point[0] - delta_x * i, end_point[1], z))
        i += 1

    i = 1
    while True:
        if w_ - delta_x * i < 0.0 or w_ - delta_x * i > w_ or h_ - delta_y * i < 0.0:
            break
        retval.append(Containers.Line(w_ - delta_x * i, 0.0, z, 0.0, h_ - delta_y * i, z))
        i += 1

    i = 0
    while True:
        if start_point[1] + delta_y * i > h_ or start_point[1] + delta_y * i < 0.0 or delta_x * i > w_:
            break
        retval.append(Containers.Line(w_, delta_y * i, z, delta_x * i, h_, z))
        i += 1

    return retval


def create_worst_case_lines(dut):
    retval = []

    z = 0.0
    w_ = dut.width
    h_ = dut.height

    retval.append(Containers.Line(0.0, 0.0, z, w_, h_, z))
    retval.append(Containers.Line(0.0, h_, z, w_, 0.0, z))
    retval.append(Containers.Line(w_ / 2.0, 0.0, z, w_ / 2.0, h_, z))
    retval.append(Containers.Line(0.0, h_ / 2.0, z, w_, h_ / 2.0, z))
    retval.append(Containers.Line(0.0, 0.0, z, 0, h_, z))
    retval.append(Containers.Line(0.0, h_, z, w_, h_, z))
    retval.append(Containers.Line(w_, h_, z, w_, 0.0, z))
    retval.append(Containers.Line(w_, 0.0, z, 0.0, 0.0, z))

    return retval

# ngocdd3
def create_point_grid(dut, grid_size,margin):
    rectangle_points = []

    border = margin*2

    w_ = dut.width-border
    h_ = dut.height-border

    w_points = int(numpy.round(w_ / grid_size))
    if w_points == 0:
        w_points = 1

    h_points = int(numpy.round(h_ / grid_size))
    if h_points == 0:
        h_points = 1

    w_real_step = w_ / w_points
    h_real_step = h_ / h_points

    # SET boundary
    for x_ in range(w_points + 1):
        for y_ in range(h_points + 1):
            rectangle_points.append(Containers.Point(x_ * w_real_step + margin, y_ * h_real_step + margin, 0.0))

    # ngocdd3
    # round conner
    point_coords = []
    for point in rectangle_points:
        point_coords.append((point.x, point.y))

    touch_points = dut.filter_points(point_coords, 'analysis_region',margin=-0.1)
    # print(touch_points)

    # Create Containers.Point objects from the coordinate list
    final_points = []
    for point in touch_points:
        final_points.append(Containers.Point(point[0], point[1], 0))
    return final_points
# ngocdd


def create_grid_from_file(dut, grid_file_path, grid_unit):
    retval = []

    w_ = dut.width
    h_ = dut.height

    with open(grid_file_path, 'r') as csvfile:
        gridreader = csv.reader(csvfile, delimiter=';', quotechar='|')

        for coord in gridreader:
            if grid_unit == "%":
                point_x = float(coord[0]) / 100 * w_
                point_y = float(coord[1]) / 100 * h_
            else:
                point_x = float(coord[0])
                point_y = float(coord[1])

            if point_x <= w_ and point_y <= h_:
                retval.append(Containers.Point(point_x, point_y, 0.0))

    return retval


def create_multifinger_tap(dut, nf, distance):
    retval = []

    w = dut.width
    h = dut.height
    d = (w ** 2 + h ** 2) ** 0.5

    # Azimuth angle to place first finger near top left corner of DUT.
    d_angle = -numpy.arctan2(h, w)

    tool_size = (float(nf) - 1.0) * distance

    if tool_size <= w:
        retval.append(Containers.Point(w / 2.0 - tool_size / 2.0, h / 2, 0.0, nf, distance, 0.0))

    if tool_size <= h:
        retval.append(Containers.Point(w / 2.0, h / 2.0 - tool_size / 2.0, 0, nf, distance, -90.0))

    if tool_size <= d:
        dir = azimuth_direction(d_angle)
        x_offset = dir[0] * tool_size / 2.0
        y_offset = dir[1] * tool_size / 2.0
        retval.append(Containers.Point((w / 2.0) - x_offset, (h / 2.0) - y_offset, 0.0, nf, distance, numpy.degrees(d_angle)))

    if len(retval) == 0:
        raise Exception("DUT is too small compared to multifinger to perform multifinger tap.")

    return retval


def create_fingermulti_swipe_diagonal(dut, num_of_fingers, finger_distance, border_width, start_points):
    """
    In diagonal multifinger swipe we want to do two swipes along both of the
    diagonals in the middle of the screen
    :param dut: the target dut
    :param num_of_fingers: number of fingers in the multifinger tool
    :param finger_distance: distance between fingers in the multifinger tool
    :param border_width: width of the border area
    :param start_points: List of start points of swipe ('top_left' or 'top_right').
    :return: List of Container.Line object(s)
    """
    dut_width = dut.width
    dut_height = dut.height
    tool_width = (num_of_fingers - 1) * finger_distance
    lines = []
    # If swipe is shorter than this an error will be raised <- the tool is too wide for the DUT
    MIN_SWIPE_LENGTH = 2 #mm

    for start_point in start_points:
        if start_point == 'top_left':
            start_corner = [0, 0]
            end_corner = [dut_width, dut_height]
            # The swipe is done diagonally. This is the angle between swipe and dut in width (x) direction
            # x-axis is the zero axis and anti-clockwise is positive direction
            azimuth = -numpy.degrees(numpy.arctan2(dut_height, dut_width))
            # We need to rotate the tool to make it perpendicular to the swipe angle
            azimuth -= 90
        elif start_point == 'top_right':
            start_corner = [dut_width, 0]
            end_corner = [0, dut_height]
            # The swipe is done diagonally. This is the angle between swipe and dut in width (x) direction
            # x-axis is the zero axis and anti-clockwise is positive direction
            azimuth = numpy.degrees(numpy.arctan2(dut_height, dut_width)) - 180
            # We need to rotate the tool to make it perpendicular to the swipe angle
            azimuth += 90
        else:
            assert False

        # The unit components of the swipe normal vector
        unit_vec = azimuth_direction(numpy.radians(azimuth))

        # Next we calculate the line the first finger has when the tool mid point
        # travels across the diagonal. This is done by moving the end and start points
        # by the amount and direction of half of the tool.
        start_x = start_corner[0] - (tool_width/2) * unit_vec[0]
        start_y = start_corner[1] - (tool_width/2) * unit_vec[1]
        start_z = 0
        end_x = end_corner[0] - (tool_width / 2) * unit_vec[0]
        end_y = end_corner[1] - (tool_width / 2) * unit_vec[1]
        end_z = 0

        # then the line needs to be clipped to take border width into account
        line_first = Containers.Line(start_x, start_y, start_z, end_x, end_y, end_z,
                                     num_of_fingers, finger_distance, azimuth)

        clip_line_to_rectangle(line_first, dut_width, dut_height, border_width)

        # Then we do the same for the for the last finger. The only difference from the first finger is
        # that the "half-tool-vector" is in opposite direction:
        start_x = start_corner[0] + (tool_width / 2) * unit_vec[0]
        start_y = start_corner[1] + (tool_width / 2) * unit_vec[1]
        start_z = 0
        end_x = end_corner[0] + (tool_width / 2) * unit_vec[0]
        end_y = end_corner[1] + (tool_width / 2) * unit_vec[1]
        end_z = 0

        # then the line needs to be clipped to take border width into account
        line_last = Containers.Line(start_x, start_y, start_z, end_x, end_y, end_z,
                                    num_of_fingers, finger_distance, azimuth)

        clip_line_to_rectangle(line_last, dut_width, dut_height, border_width)

        # Depending on the dut shape we need to either limit the start or end of the
        # first finger line by the last finger line
        if dut_width > dut_height:
            line_first.end_x = line_last.end_x - tool_width*unit_vec[0]
            line_first.end_y = line_last.end_y - tool_width*unit_vec[1]
        else: # dut_width <= dut_height
            line_first.start_x = line_last.start_x - tool_width*unit_vec[0]
            line_first.start_y = line_last.start_y - tool_width*unit_vec[1]

        # Checking if the resulting line is long enough
        line_length = line_first.length()
        if line_length > MIN_SWIPE_LENGTH:
            lines.append(line_first)

    return lines


def create_multifinger_swipe(dut, nf, distance):
    retval = []

    w = dut.width
    h = dut.height

    tool_size = (float(nf) - 1.0) * distance

    if tool_size < w:
        sx = w / 2.0 - tool_size / 2.0
        ex = sx
        sy = h
        ey = 0

        retval.append(Containers.Line(sx, sy, 0, ex, ey, 0, nf, distance, 0.0))

    if tool_size < h:
        sx = 0
        ex = w
        sy = h / 2.0 - tool_size / 2.0
        ey = sy

        retval.append(Containers.Line(sx, sy, 0, ex, ey, 0, nf, distance, -90.0))

    retval += create_fingermulti_swipe_diagonal(dut, nf, distance, 0, ['top_left'])

    if len(retval) == 0:
        raise Exception("DUT is too small compared to multifinger to perform multifinger swipe.")

    return retval


def create_separation(dut, sum_of_tip_radii):
    retval = []

    w = dut.width
    h = dut.height
    d = (w ** 2 + h ** 2) ** 0.5
    nf = 2

    d_angle = -numpy.degrees(numpy.arctan2(h, w))

    # Note that Two-finger has first finger offset equal to separation / 2 as the offset is distance from
    # the middle of two-finger tool to the first finger. This is for compatibility with the multifinger concept.

    for distance in range(13):
        separation = distance + sum_of_tip_radii

        tool_size = (float(nf) - 1.0) * separation

        if tool_size > w or tool_size > h:
            raise Exception("Separation test: Tool is bigger than DUT! Test is not able to continue")

        retval.append(Containers.Point(w / 2.0, h / 2, 0.0, nf, separation, 0.0, 0))

    for distance in range(13):
        separation = distance + sum_of_tip_radii
        tool_size = (float(nf) - 1.0) * separation

        if tool_size > w or tool_size > h:
            raise Exception("Separation test: Tool is bigger than DUT! Test is not able to continue")

        retval.append(Containers.Point(w / 2.0, h / 2.0, 0, nf, separation, -90.0, 0))

    for distance in range(16):
        separation = distance + sum_of_tip_radii
        tool_size = (float(nf) - 1.0) * separation

        if tool_size > d:
            raise Exception("Separation test: Tool is bigger than DUT! Test is not able to continue")

        retval.append(Containers.Point((w / 2.0), (h / 2.0), 0.0, nf, separation, d_angle, 0))

    return retval

# ngocdd3
# ==========================
def create_vertical_horizontal_line_grid_custom(dut, grid_size,margin):
    retval = []
    z = 0.0

    #ngocdd3
    border = margin*2

    w_ = dut.width - border
    h_ = dut.height - border

    w_points = int(numpy.round(w_ / grid_size))
    if w_points == 0: w_points = 1

    h_points = int(numpy.round(h_ / grid_size))
    if h_points == 0: h_points = 1

    w_real_step = (w_) / w_points
    h_real_step = (h_) / h_points

    # Horizontal lines
    for y_ in range(h_points + 1):
        retval.append(Containers.Line(0.0, y_ * h_real_step+border, z, w_, y_ * h_real_step+border, z))
    # Vertical lines
    for x_ in range(w_points + 1):
        retval.append(Containers.Line(x_ * w_real_step+border, 0.0, z, x_ * w_real_step+border, h_, z))

    # change swipe aree follow SVG analysis_region
    line_coords = []
    for point in retval:
        line_coords.append([point.start_x, point.start_y, point.end_x, point.end_y])

    swipe_line = dut.filter_lines(line_coords,'bounding_box',margin)


    # Create Containers.Point objects from the coordinate list
    final_lines = []
    for line_list in line_coords:
        for line in line_list:
            final_lines.append(Containers.Line(line[0], line[1], z, line[2], line[3], z))
    return final_lines


def create_diagonal_line_grid_custom(dut, dist,margin):
    retval = []
    w_ = dut.width
    h_ = dut.height
    z = 0.0

    delta_x = dist / (math.sin(math.atan(h_ / w_)))
    delta_y = delta_x * (h_ / w_)

    # Horizontal lines
    start_point = [0.0, 0.0]
    end_point = [w_, h_]
    i = 1

    while True:
        if start_point[0] + delta_x * i > w_ or start_point[0] + delta_x * i < 0.0 or end_point[1] - delta_y * i < 0.0:
            break
        retval.append(Containers.Line(delta_x * i, 0.0, z, end_point[0], end_point[1] - delta_y * i, z))
        i += 1

    i = 0
    while True:
        if start_point[1] + delta_y * i > h_ or start_point[1] + delta_y * i < 0.0 or end_point[0] - delta_x * i < 0.0:
            break
        retval.append(Containers.Line(0.0, delta_y * i, z, end_point[0] - delta_x * i, end_point[1], z))
        i += 1

    i = 1
    while True:
        if w_ - delta_x * i < 0.0 or w_ - delta_x * i > w_ or h_ - delta_y * i < 0.0:
            break
        retval.append(Containers.Line(w_ - delta_x * i, 0.0, z, 0.0, h_ - delta_y * i, z))
        i += 1

    i = 0
    while True:
        if start_point[1] + delta_y * i > h_ or start_point[1] + delta_y * i < 0.0 or delta_x * i > w_:
            break
        retval.append(Containers.Line(w_, delta_y * i, z, delta_x * i, h_, z))
        i += 1

    # change swipe aree follow SVG analysis_region
    line_coords = []
    for point in retval:
        line_coords.append([point.start_x, point.start_y, point.end_x, point.end_y])
    # print(line_coords)
    # print('============================')
    swipe_line = dut.filter_lines(line_coords, 'analysis_region', margin)
    # print(swipe_line)
    #
    # Create Containers.Point objects from the coordinate list
    final_lines = []
    for line_list in swipe_line:
        for line in line_list:
            # print(line)
            final_lines.append(Containers.Line(line[0], line[1], z, line[2], line[3], z))
    return final_lines


def create_worst_case_lines_custom(dut,margin,area,edge):
    if (area == 'edge'):
        retval = []

        z = -0.1
        w_ = dut.width - margin
        h_ = dut.height - margin

        retval.append(Containers.Line(margin, margin, z, margin, h_, z))
        retval.append(Containers.Line(margin, h_, z, w_, h_, z))
        retval.append(Containers.Line(w_ - z, h_, z, w_ - z, margin, z))
        retval.append(Containers.Line(w_, margin, z, margin, margin, z))


    elif (area == 'center'):
        retval = []

        margin = margin * 2

        z = -0.1
        w_ = dut.width - margin
        h_ = dut.height - margin

        retval.append(Containers.Line(margin, margin, z, w_, h_, z))
        retval.append(Containers.Line(margin, h_, z, w_, margin, z))
        retval.append(Containers.Line(w_ / 2.0, margin, z, w_ / 2.0, h_, z))
        retval.append(Containers.Line(margin, h_ / 2.0, z, w_, h_ / 2.0, z))
        retval.append(Containers.Line(margin, margin, z, margin, h_, z))
        retval.append(Containers.Line(margin, h_, z, w_, h_, z))
        retval.append(Containers.Line(w_ - z, h_, z, w_ - z, margin, z))
        retval.append(Containers.Line(w_, margin, z, margin, margin, z))

    elif (area == 'worstcases'):
        retval = []

        z = -0.1
        org_w = dut.width
        org_h = dut.height

        w_ = dut.width - margin
        h_ = dut.height - margin

        #diogil left to right
        retval.append(Containers.Line(margin+edge, margin+edge, z, w_, h_, z))
        retval.append(Containers.Line( margin, h_, z, w_, margin+edge, z))
        retval.append(Containers.Line(org_w / 2.0, margin+edge, z, org_w / 2.0, h_, z))
        retval.append(Containers.Line(margin, org_h / 2.0, z, w_, org_h / 2.0, z))
        retval.append(Containers.Line(margin, margin+edge, z, margin, h_, z))
        retval.append(Containers.Line( margin, h_, z, w_, h_, z))
        retval.append(Containers.Line(w_ - z, h_, z, w_ - z, margin+edge, z))
        retval.append(Containers.Line(w_, margin+edge, z, margin+edge, margin+edge, z))

    return retval
# =========================
# ngocdd3
def create_point_grid_custom(dut, grid_size, margin,edge,height):
    rectangle_points = []

    border = margin * 2

    w_ = dut.width - border
    h_ = dut.height - border

    w_points = int(numpy.round(w_ / grid_size))
    if w_points == 0:
        w_points = 1

    h_points = int(numpy.round(h_ / height))
    if h_points == 0:
        h_points = 1

    w_real_step = w_ / w_points
    h_real_step = h_ / h_points

    for x_ in range(w_points + 1):
        for y_ in range(h_points + 1):
            leftEdge = x_ * w_real_step + margin
            RightEdge = w_ - edge
            if(leftEdge<edge or leftEdge>RightEdge):
                    rectangle_points.append(Containers.Point(x_ * w_real_step + margin, y_ * h_real_step + margin, 0.0))

    # ngocdd3
    # round conner
    point_coords = []
    for point in rectangle_points:
        point_coords.append((point.x, point.y))
    touch_points = dut.filter_points(point_coords, 'analysis_region', margin)
    # print(touch_points)

    # Create Containers.Point objects from the coordinate list
    final_points = []
    for point in touch_points:
        final_points.append(Containers.Point(point[0], point[1], 0))
    return final_points
# ngocdd

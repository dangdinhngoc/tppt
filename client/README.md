# Setting up for development

## Windows

- Make sure you have Git installed and configured
- [Install Python 3.5.1 32-bit](https://www.python.org/downloads/windows/)
  - Make sure that path is correct: In command prompt `py -3.5-32` should launch the required python version.
- Open command prompt (cmd.exe, not powershell or any other) and navigate to path where to develop
- Clone the repository: `git clone git@git.optofidelity.net:robotics/tnt_client_gt.git`
- Go to the repository `cd tnt_client_gt`
- Run the develop_env.bat `develop_env.bat`

The environment should now be complete. Start the client by issuing

`python -i main.py`

This imports the different clients and creates a TnTRobotClient 'robot' with name-parameter 'Robot1'

Now you can issue commands through the 'robot'-object or create a TnTDUTClient if there are duts on the server side and issue gestures and commands through them.

To later run from command line, just first activate the virtual environment by running the `activate.bat` from `venv\Scripts` and then you can run the python code.

## OS X, Linux

TODO

## Documentation

You can generate API documentation in HTML and PDF formats by running `build_doc.bat` at the same folder as `develop_env.bat`.

You must have run `develop_env.bat` at least once before `build_doc.bat`

## PyCharm

[PyCharm](https://www.jetbrains.com/pycharm/) is the recommended IDE. To get started, open the repository directory in PyCharm. Then open File -> Settings -> Project -> Project Interpreter. Click the Project Interpreter down and select Show All. Locate the previously created venv.
In case PyCharm appears to be slow, the virus scanner might be interfering with it. In this case PyCharm should be added in scanner's excluded application list.
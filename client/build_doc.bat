call venv\Scripts\activate.bat

python doc/autodoc.py || goto :error
python doc/autodoc_pdf.py
copy tnt_client_api.html doc
copy tnt_client_api.pdf doc
del tnt_client_api.html
del tnt_client_api.pdf
call venv\Scripts\deactivate.bat
goto :EOF

:error
call venv\Scripts\deactivate.bat
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
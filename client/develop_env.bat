py -3.5-32 -m venv venv
call venv\Scripts\activate.bat
python -m pip install --upgrade pip
python -m pip install -U setuptools wheel
pip install -r requirements.txt
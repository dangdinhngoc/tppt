'''
Auto-document generator for Python 3.
Some features:
- Uses runtime reflection to parse package and module structure.
- Aims to work with reStructuredText formatting.
- Parse general description and arguments from class method docstring.
- Detects if method argument is not referenced in docstring.
'''

import os
import importlib

class DocstringParser:
    """
    Parses some annotations from a docstring by assuming reStructuredText formatting.
    """

    def __init__(self, docstring, line_break='\n'):
        lines = docstring.splitlines()

        self.descr = ""
        self.parameters = {}
        self.returns = None
        self.example = None

        line_ix = 0
        state = "DEFAULT"
        state_param = ""
        add_line_break = False

        while line_ix < len(lines):
            line = lines[line_ix]
            line_ix += 1
            stripped_line = line.strip()

            try:
                if stripped_line.startswith(":"):
                    if stripped_line.startswith(":param"):
                        state = "PARAM"
                        parts = stripped_line[6:].split(":", 1)

                        text = parts[1]
                        state_param = parts[0].strip()
                        self.parameters[state_param] = text.strip()

                    elif stripped_line.startswith(":return"): # Can be ":return:" or ":returns:"
                        state = "RETURN"
                        parts = stripped_line.split(":", 2)

                        self.returns = parts[2].strip()
                    elif stripped_line.startswith(":example:"):
                        self.example = stripped_line[9:]

                        while line_ix < len(lines):
                            line = lines[line_ix]
                            stripped_line = line.strip()

                            if stripped_line.startswith(":"):
                                break
                            else:
                                line_ix += 1

                            self.example += line_break + stripped_line

                # If we have empty line and description exists, we'll add line break when appending next line of text
                elif len(stripped_line) == 0 and len(self.descr) != 0:
                    add_line_break = True

                else:
                    if state == "PARAM":
                        # We are in @param block and want to add a space and content of new line for the parameter description
                        self.parameters[state_param] += " " + stripped_line
                    elif state == "RETURN":
                        self.returns += " " + stripped_line
                    else:
                        # Add line break if previous line was empty and we are composing description
                        if add_line_break:
                            self.descr += line_break
                            add_line_break = False
                        self.descr += stripped_line + " "
                        state = "DEFAULT"
            except Exception:
                print("Error parsing line: " + line)

        self.descr = self.descr.strip()

        if self.example:
            self.example = self.example.strip()

def get_classnames_from_file( path):
    classnames = []

    file = open(path, "r")
    s = file.read()
    file.close()

    lines = s.split("\n")
    for line in lines:
        if line[:6] == "class ":
            line = line[6:]
            line = line.split("(")[0]
            classnames.append(line)

    return classnames

def get_functions_properties_from_class(cls):
    fs, ps = [], []

    # Build a list of class methods and properties ignoring ones inherited from base classes.
    dir_cls = dir(cls)
    dir_cls_base = []

    for base in cls.__bases__:
        dir_cls_base.extend(dir(base))

    all = [item for item in dir_cls if item not in dir_cls_base]

    for v in all:
        if v[0] == "_":
            # private
            continue

        if isinstance(getattr(cls, v), property):
            ps.append(v)
        elif callable(getattr(cls, v)):
            fs.append(v)
    return fs, ps

def parse_markdown(filename):
    """
    Parses Markdown file and generates HTML.
    N.B. Only supports a very limited Markdown syntax subset.
    """

    with open(filename) as file:
        data = file.read()

    lines = data.split('\n')

    monospace = False

    html = ""

    for line in lines:
        if line.startswith('#'):
            html += "<h1>" + line[1:] + "</h1>"
        elif line.startswith('```'):
            monospace = not monospace
            if monospace:
                html += "<p class='codeblock'>"
            else:
                html += "</p>"
        elif len(line) > 0:
            if monospace:
                html += line + "<br/>"
            else:
                html += "<p>" + line + "</p>"

    return html

class ModuleDoc:
    """
    Create HTML documentation for a single Python module.
    """

    def __init__(self):
        self.html = ""
        self.num_missing = 0

    def parse_class_function(self, f, f_name):
        self.html += "<table>"

        self.html += "  <tr>"
        self.html += "    <th class='function'>Method name</th>"
        self.html += "    <th class='function'> <p class='code'>" + f_name + "</p></th>"
        self.html += "  </tr>"

        self.html += "  <tr>"
        self.html += "    <th>Description</th>"

        parser = None

        # Function general description.
        if f.__doc__ is None or len(f.__doc__.strip()) == 0:
            self.html += "    <td style='color:red'>Missing</td>"
            self.num_missing += 1

            # Can't continue if there is no docstring
            self.html += "  </tr>"
            self.html += "</table>"
            return
        else:
            parser = DocstringParser(f.__doc__, "<br/>")

            self.html += "    <td>" + parser.descr + "</td>"

        self.html += "  </tr>"

        # Get a list of function parameters
        parameters = []

        try:
            parameters = f.__code__.co_varnames[:f.__code__.co_argcount]
        except AttributeError:
            pass

        self.html += "  <tr>"
        self.html += "    <th>Arguments</th>"

        self.html += "    <td>"

        # Function parameters
        if len(parameters) == 0 or (len(parameters) == 1 and parameters[0] == 'self'):
            # There are no parameters except "self".
            self.html += "None"
        else:
            for p in parameters:
                # Don't document "self"
                if p == "self":
                    continue

                self.html += "<p class='code'>" + p + "</p>"

                # Check if parameter is mentioned in parsed docstring.
                if p in parser.parameters and len(parser.parameters[p].strip()) > 0:
                    self.html += "<p class='description'>"
                    self.html += parser.parameters[p]
                    self.html += "</p>"
                else:
                    self.html += "<p class='description' style='color:red'>Missing description</p>"
                    self.num_missing += 1

        self.html += "    </td>"
        self.html += "  </tr>"

        # Function return value
        if parser.returns is not None:
            self.html += "  <tr>"
            self.html += "    <th>Returns</th>"

            if len(parser.returns.strip()) == 0:
                self.html += "    <td style='color:red'>Missing</td>"
                self.num_missing += 1
            else:
                self.html += "    <td>" + parser.returns + "</td>"

            self.html += "  </tr>"

        # Example
        if parser.example is not None:
            self.html += "  <tr>"
            self.html += "    <th>Example</th>"

            self.html += "<td><p class='codeblock'>" + parser.example + "</p></td>"

            self.html += "  </tr>"

        self.html += "</table>"

    def parse_class_property(self, p, p_name):
        self.html += "<table>"

        self.html += "  <tr>"
        self.html += "    <th class='property'>Property name</th>"
        self.html += "    <th class='property'><p class='code'>" + p_name + "</p></th>"
        self.html += "  </tr>"

        self.html += "  <tr>"
        self.html += "    <th>Description</th>"

        if p.__doc__ is None or len(p.__doc__.strip())==0:
            self.html += "    <td style='color:red'>Missing</td>"
            self.num_missing += 1
        else:
            self.html += "    <td>" + p.__doc__ + "</td>"

        self.html += "  </tr>"

        self.html += "</table>"

    def create(self, package_name, module_name):
        self.html = ""

        importname = package_name + "." + module_name
        module = importlib.import_module(importname, ModuleDoc.__module__)

        #print("Module:", module)
        self.html += "<h1>Module: {}</h1>\n".format(module.__name__)

        classnames = get_classnames_from_file(os.path.realpath(package_name + "/" + module_name + ".py"))

        for classname in classnames:
            cls = getattr(module, classname)
            print("API: ", cls.__name__)
            self.html += "<h2>Class: {}</h2>\n".format(cls.__name__)
            functions, properties = get_functions_properties_from_class(cls)

            if cls.__doc__ is not None:
                parser = DocstringParser(cls.__doc__, "<br/>")

                if len(parser.descr) > 0:
                    self.html += parser.descr

                if parser.example is not None:
                    self.html += "<p>Example:</p>"
                    self.html += "<p class='codeblock'>" + parser.example + "</p>"

            for f_name in functions:
                f = getattr(cls, f_name)
                self.parse_class_function(f, f_name)

            for p_name in properties:
                p = getattr(cls, p_name)
                self.parse_class_property(p, p_name)

def create_doc(intro_page_name, package_name, output_path, output_name, style_file):

    html = ""

    html += "<!DOCTYPE html>"
    html += "<html>"
    html += "<head>"

    # Dump style to html file to get self-contained document file
    with open(style_file) as file:
        html += "<style>" + file.read() + "</style>"

    #html += "<link rel='stylesheet' type='text/css' href='style.css'>"
    html += "</head>"

    html += "<body>"

    html += parse_markdown(intro_page_name)

    html += "<div>"

    files = os.listdir(package_name)

    success = True

    for filename in files:
        if filename.endswith(".py"):
            module_doc = ModuleDoc()
            module_doc.create(package_name, filename[:-3])

            html += module_doc.html

            if module_doc.num_missing != 0:
                print("Module " + filename + " has " + str(module_doc.num_missing) + " missing parts")
                success = False

    html += "</div>"
    html += "</body>"
    html += "</html>"

    file = open(os.path.join(output_path, output_name + ".html"), "w")
    file.write(html)
    file.close()

    return success

if __name__ == "__main__":
    data_dir = os.path.join(os.path.dirname(__file__), 'data')

    if not create_doc(os.path.join(data_dir, "intro.md"), "tntclient", ".", "tnt_client_api", os.path.join(data_dir, "style.css")):
        raise Exception("Error in document generation.")
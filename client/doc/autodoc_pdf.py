'''
Auto-document generator for Python 3.
Some features:
- Uses runtime reflection to parse package and module structure.
- Aims to work with reStructuredText formatting.
- Parse general description and arguments from class method docstring.
- PDF output.

This module utilizes functions from autodoc.py module and has some overlap regarding docstring parsing.
The reason this module exists is that most / all HTML -> PDF converters produce bad quality especially with
tables. This implementation also allows better page layout control.
'''

from fpdf import FPDF, HTMLMixin
import os
from autodoc import get_classnames_from_file, get_functions_properties_from_class, DocstringParser
import importlib

class ModuleDocPdf(FPDF, HTMLMixin):
    '''
    Create module PDF documentation from docstrings.
    '''

    def __init__(self):
        FPDF.__init__(self)
        HTMLMixin.__init__(self)

        self.data_dir = os.path.join(os.path.dirname(__file__), 'data')

        # Enable getting total page count in footer.
        self.alias_nb_pages()

        self.text_area_width = (self.w - self.l_margin - self.r_margin)

        # Load custom font.
        self.add_font('Calibri', '', os.path.join(self.data_dir, 'calibri.ttf'), uni=True)
        self.add_font('Calibri', 'B', os.path.join(self.data_dir, 'calibrib.ttf'), uni=True)

        self.num_module_headers = 0
        self.num_class_headers = 0

        # Number of missing docstring parts.
        self.num_missing = 0

        # Set document margins.
        self.set_margins(10, 25, 10)

        # Define font names used by document.
        self.normal_font = "Calibri"
        self.monospace_font = "Courier"

        self.set_title("TnT Client Python Reference")
        self.set_subject("TnT Client Python Reference")
        self.set_author("OptoFidelity")
        self.set_creator("OptoFidelity")
        self.set_keywords("TnT, Client, Python, API")

    def add_title_page(self):
        self.add_page()
        self.image(os.path.join(self.data_dir, 'of_title.jpg'), x=self.l_margin, y=self.t_margin, w=self.text_area_width)
        self.set_text_color(0, 82, 148)
        self.set_font(self.normal_font, size=44)
        self.set_y(170)
        self.cell(0, 44, "OptoFidelity Touch and Test")
        self.set_text_color(0, 0, 0)
        self.set_font(self.normal_font, size=36)
        self.set_y(200)
        self.cell(0, 36, "Python Client Reference")

    def parse_markdown(self, filename):
        '''
        Parses Markdown file and generates PDF page.
        N.B. Only supports a very limited Markdown syntax subset.
        '''

        self.add_page()

        with open(filename) as file:
            data = file.read()

        lines = data.split('\n')

        monospace = False

        for line in lines:
            if line.startswith('#'):
                self.add_header(line[1:])
            elif line.startswith('```'):
                monospace = not monospace
                self.ln(5) # Some space around monospace text
            elif len(line) > 0:
                if monospace:
                    self.add_paragraph(line, font_name=self.monospace_font)
                else:
                    self.add_paragraph(line)

    def wrap_text(self, text, max_line_width):
        '''
        Wrap given text according to maximum line width.
        :return: List of lines that obey maximum width.
        '''

        lines = []
        words = text.split(' ')
        line = ""

        for w in words:

            if self.get_string_width(line + ' ' + w) < max_line_width:
                line += w + ' '
            else:
                lines.append(line)
                line = w + ' '

        lines.append(line)

        return lines

    def parse_class_function(self, f, f_name):
        '''
        Create documentation for class function.
        '''

        self.add_description("Method:  ", f_name)

        self.add_paragraph("Description:")

        parser = None

        # Function general description.
        if f.__doc__ is None or len(f.__doc__.strip()) == 0:
            self.num_missing += 1

            # Can't continue if there is no docstring
            return
        else:
            parser = DocstringParser(f.__doc__)

            self.add_paragraph(parser.descr, indent=True)

        # Get a list of function parameters
        parameters = []

        try:
            parameters = f.__code__.co_varnames[:f.__code__.co_argcount]
        except AttributeError:
            pass

        self.add_paragraph("Arguments:")

        # Function parameters
        if len(parameters) == 0 or (len(parameters) == 1 and parameters[0] == 'self'):
            # There are no parameters except "self".
            self.add_paragraph("None", indent=True)
        else:
            for p in parameters:
                # Don't document "self"
                if p == "self":
                    continue

                # Check if parameter is mentioned in parsed docstring.
                if p in parser.parameters and len(parser.parameters[p].strip()) > 0:
                    self.add_row(p, parser.parameters[p])
                else:
                    self.num_missing += 1

        # Function return value
        if parser.returns is not None:
            self.add_paragraph("Returns:")

            if len(parser.returns.strip()) == 0:
                self.num_missing += 1
            else:
                self.add_paragraph(parser.returns, indent=True)

        # Example
        if parser.example is not None:
            self.add_paragraph("Example:")
            self.add_paragraph(parser.example, indent=True)

        self.ln(5)

    def parse_class_property(self, p, p_name):
        '''
        Create documentation for class property.
        '''

        if p.__doc__ is None or len(p.__doc__.strip())==0:
            self.num_missing += 1
        else:
            parser = DocstringParser(p.__doc__)

            self.add_row(p_name, parser.descr)

    def parse_module(self, package_name, module_name):
        '''
        Create documentation for entire Python module that is part of a package.
        '''

        importname = package_name + "." + module_name
        module = importlib.import_module(importname, ModuleDocPdf.__module__)

        self.add_module_header(module.__name__)

        classnames = get_classnames_from_file(os.path.realpath(package_name + "/" + module_name + ".py"))

        # Parse each class in the module.
        for classname in classnames:
            cls = getattr(module, classname)
            print("API: ", cls.__name__)
            self.add_class_header(cls.__name__)
            functions, properties = get_functions_properties_from_class(cls)

            # Parse class docstring.
            if cls.__doc__ is not None:
                parser = DocstringParser(cls.__doc__)

                if len(parser.descr) > 0:
                    self.add_paragraph(parser.descr)
                    self.ln(3)

                if parser.example is not None:
                    self.add_paragraph("Example:")
                    self.add_paragraph(parser.example, font_name=self.monospace_font, indent=True)
                    self.ln(3)

            # Document class functions.
            for f_name in functions:
                f = getattr(cls, f_name)
                self.parse_class_function(f, f_name)

            # Document class properties.
            if len(properties) != 0:
                self.add_description("Properties:", None)

            for p_name in properties:
                p = getattr(cls, p_name)
                self.parse_class_property(p, p_name)

    def footer(self):
        '''
        Define page footer.
        '''

        # No footer in title page.
        if self.page_no() == 1:
            return

        # Page number.
        self.set_y(-10)
        self.set_font(self.normal_font, size=8)
        self.cell(0, 10, "Page " + str(self.page_no()) + " / {nb}", align='C')

        self.set_y(-self.b_margin)
        self.set_font(self.normal_font, size=10)
        self.set_text_color(255, 255, 255)

        w = (self.w - self.l_margin - self.r_margin) / 3

        # OptoFidelity footer block.
        self.set_fill_color(238, 49, 32)
        self.cell(w, 10, "COMPANY CONFIDENTIAL", fill=1, align="C")
        self.set_fill_color(102, 196, 48)
        self.cell(w, 10, "www.optofidelity.com", fill=1, align="C")
        self.set_fill_color(23, 111, 192)
        self.cell(w, 10, "sales@optofidelity.com", fill=1, align="C")

        self.set_fill_color(255, 255, 255)
        self.set_text_color(0, 0, 0)

    def header(self):
        '''
        Define page header.
        '''

        # No header in title page.
        if self.page_no() == 1:
            return

        # OptoFidelity logo.
        self.image(os.path.join(self.data_dir, 'of_logo.png'), x=5, y=5, w=40)

    def add_paragraph(self, text, font_name=None, indent=False):
        '''
        Paragraph is just multi-line text.
        '''

        if font_name is None:
            self.set_font(self.normal_font, size=10)
        else:
            self.set_font(font_name, size=10)

        x = self.get_x()

        if indent:
            self.set_x(x + 2.0)

        self.multi_cell(0, 5, text, border=0, align='L')
        self.set_x(x)

    def add_description(self, name, text):
        '''
        Add description that consists of bold name and mono-space text.
        '''

        self.set_font(self.normal_font, size=10, style='B')
        self.cell(self.get_string_width(name), 5, name, border=0)
        if text is not None:
            self.set_font(self.monospace_font, size=10)
            self.set_fill_color(200, 200, 255)
            self.cell(self.get_string_width(text) + 3, 5, text, border=0, ln=1, fill=1)
            self.set_fill_color(255, 255, 255)
        else:
            self.ln()

    def determine_font_size(self, text, font_name, start_font_size, max_width):
        '''
        Determine font size that makes given text fit to given maximum width.
        '''
        font_size = start_font_size

        while font_size > 6:
            self.set_font(font_name, size=font_size)

            if self.get_string_width(text) <= max_width:
                return font_size

            font_size -= 1

    def add_row(self, text1, text2):
        '''
        Add two-column table row. These can be added one after another to create an entire table.
        First column uses mono-space font and second column uses normal font.
        First column content must be single-line. Second column content can span multiple lines.
        Intended use-case is function parameter descriptions.
        '''

        # Column widths obey the golden ratio.
        golden_ratio = 1.61803398875
        col_width1 = self.text_area_width / (golden_ratio + 1)
        col_width2 = self.text_area_width * golden_ratio / (golden_ratio + 1)
        col_height = 5

        lines = self.wrap_text(text2, col_width2)

        total_height = col_height * len(lines)

        # Position fiddling below only works if row does not break page.
        if self.h - self.b_margin - self.t_margin - self.get_y() - total_height <= 1:
            self.add_page()

        x = self.get_x()
        y = self.get_y()

        # Create column borders.
        self.cell(col_width1, total_height, "", border=1, ln=0)
        self.cell(col_width2, total_height, "", border=1, ln=0)

        self.set_x(x)
        self.set_y(y)

        # Column 1 text. Choose font so that text fits in column.
        font_size = self.determine_font_size(text1, self.monospace_font, 10, col_width1)
        self.set_font(self.monospace_font, size=font_size)
        self.cell(self.get_string_width(text1), col_height, text1, border=0, ln=1)

        self.set_y(y)

        self.set_font(self.normal_font, size=10)

        # Column 2 text.
        for line in lines:
            self.set_x(x + col_width1)
            self.cell(self.get_string_width(line), col_height, line, border=0, ln=1)

    def add_header(self, text):
        '''
        Add big generic header.
        '''

        self.num_module_headers += 1
        text_size = 14

        prefixed_text = str(self.num_module_headers) + text

        self.set_font(self.normal_font, style='B', size=text_size)
        self.cell(self.get_string_width(prefixed_text), text_size, prefixed_text, border=0, ln=1)

    def add_module_header(self, text):
        '''
        Add big header for module documentation.
        '''

        self.num_module_headers += 1
        self.num_class_headers = 0
        text_size = 14

        prefix = str(self.num_module_headers) + " Module: "

        self.set_font(self.normal_font, style='B', size=text_size)
        self.cell(self.get_string_width(prefix), text_size, prefix, border=0)
        self.set_font(self.monospace_font, size=text_size)
        self.cell(self.get_string_width(text), text_size, text, border=0, ln=1)

    def add_class_header(self, text):
        '''
        Add small header for class documentation.
        '''

        self.num_class_headers += 1
        text_size = 12

        prefix = str(self.num_module_headers) + "." + str(self.num_class_headers) + " Class: "

        self.set_font(self.normal_font, style='B', size=text_size)
        self.cell(self.get_string_width(prefix), text_size, prefix, border=0)
        self.set_font(self.monospace_font, size=text_size)
        self.cell(self.get_string_width(text), text_size, text, border=0, ln=1)


def create_doc(package_name, output_path, output_name):

    files = os.listdir(package_name)

    module_doc = ModuleDocPdf()

    module_doc.add_title_page()

    module_doc.parse_markdown(os.path.join(module_doc.data_dir, 'intro.md'))

    for filename in files:
        if filename.endswith(".py"):
            # Each module starts new page.
            module_doc.add_page()

            module_doc.parse_module(package_name, filename[:-3])

    file_name = os.path.join(output_path, output_name + ".pdf")
    module_doc.output(file_name)

if __name__ == "__main__":
    create_doc("tntclient", ".", "tnt_client_api")


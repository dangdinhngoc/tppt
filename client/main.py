from tntclient.tnt_robot_client import TnTRobotClient
from tntclient.tnt_dut_client import TnTDUTClient
from tntclient.tnt_dutphysicalbutton_client import TnTDutPhysicalButtonClient
from tntclient.tnt_camera_client import TnTCameraClient
from tntclient.tnt_tip_client import TnTTipClient
from tntclient.tnt_client import TnTDutPoint

robot = TnTRobotClient("Robot1")
from .tnt_client import TnTClientObject, RequestError
from . import logger

log = logger.get_logger(__name__)


class TnTDutPhysicalButtonClient(TnTClientObject):
    """This class implements methods to control TnT Server dutphysicalbutton resources."""
    def __init__(self, name, workspace=TnTClientObject.default_workspace, host="127.0.0.1", port=8000):
        TnTClientObject.__init__(self, host, port, workspace, 'dutphysicalbutton', name)

    @property
    def parent(self):
        """ Reads DUT button parent from the server. """
        links = self._GET("")["_links"]
        for link in links:
            if link["rel"] == "parent":
                return link["resourcename"]

    def press(self):
        """ Press the button. """
        return self._PUT("press", {})

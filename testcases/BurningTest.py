from TPPTcommon.Node import *
import TPPTcommon.containers as Containers
from scriptpath import join_script_root_directory
from os import path
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
from datetime import datetime
import random
logger = logging.getLogger(__name__)


class BurningTest(TestStep):
    """
    Burning test is used in development to run long test sequences that test
    robot functionality and stability.
    """

    def __init__(self, context):
        super().__init__('Burning test')

        self.context = context

        self.controls.testing_time = "0.1"
        self.controls.info['testing_time'] = {'label': 'Approx testing time (hours)'}

        self.controls.dut1 = "Dut1"
        self.controls.info['dut1'] = {'label': 'Dut1'}

        self.controls.dut2 = "Dut2"
        self.controls.info['dut2'] = {'label': 'Dut2'}

        self.controls.robot_min_x = -5
        self.controls.info['robot_min_x'] = {'label': 'Minimum robot x position'}

        self.controls.robot_max_x = 350
        self.controls.info['robot_max_x'] = {'label': 'Maximum robot x position'}

        self.controls.robot_min_y = 0
        self.controls.info['robot_min_y'] = {'label': 'Minimum robot y position'}

        self.controls.robot_max_y = 350
        self.controls.info['robot_max_y'] = {'label': 'Maximum robot y position'}

        self.controls.min_acceleration = 100
        self.controls.info['min_acceleration'] = {'label': 'Robot minimum acceleration'}

        self.controls.max_acceleration = 800
        self.controls.info['max_acceleration'] = {'label': 'Robot maximum acceleration'}

        self.controls.min_velocity = 100
        self.controls.info['min_velocity'] = {'label': 'Robot minimum velocity'}

        self.controls.max_velocity = 250
        self.controls.info['max_velocity'] = {'label': 'Robot maximum velocity'}

        # List of boolean controls for enabling various test cases in burning test.
        self.test_control_names = []

        def insert_test(control_name, label):
            self.test_control_names.append(control_name)
            setattr(self.controls, control_name, True)
            self.controls.info[control_name] = {'label': label}

        insert_test('movement', 'Movements (robot)')
        insert_test('two_finger', 'Two-finger')
        insert_test('tap', 'Taps')
        insert_test('swipe', 'Swipes')
        insert_test('press', 'Presses')
        insert_test('drag', 'Drags')
        insert_test('random', 'Random gestures')

        insert_test('pinch', 'Pinches')
        insert_test('drumroll', 'Drumroll')
        insert_test('compass', 'Compass')
        insert_test('compass_tap', 'Compass tap')
        insert_test('touch_and_tap', 'Touch and tap')
        insert_test('line_tap', 'Line tap')

        self.controls.find_object = False
        self.controls.info['find_object'] = {'label': 'Find Object (dut)'}
        self.test_control_names.append("find_object")

        self.controls.object_image = "test_image.png"
        self.controls.object_shm = "of_logo.shm,robot.shm,atom.shm,coffee.shm"
        self.controls.info['object_image'] = {'label': 'Image to show on DUT'}
        self.controls.info['object_shm'] = {'label': 'Image SHMs to detect image with'}

        self.controls.search_text = False
        self.test_control_names.append("search_text")
        self.controls.info['search_text'] = {'label': 'Search Text Pattern (dut)'}

        self.controls.text_string = "OptoFidelity,Touch Panel Performance Test"
        self.controls.text_image = "test_image.png"
        self.controls.info['text_string'] = {'label': 'Text to Search'}
        self.controls.info['text_image'] = {'label': 'Image containing text'}

        # Z-coordinate where robot is moved during movement test.
        # This should be something the robot can reach even if there is a tip attached.
        # TODO: Use active tip length when obtainable from TnTTipClient.
        self.robot_z = -20

        self.duts = None

    def test_movement(self):
        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        movement_count = 6
        for index in range(movement_count):
            # Set speeds
            velocity = random.uniform(velocity_min, velocity_max)
            acceleration = random.uniform(acceleration_min, acceleration_max)

            robot.set_speed(velocity, acceleration)
            robot.move(self.controls.robot_min_x, self.controls.robot_min_y, self.robot_z)
            robot.move(self.controls.robot_max_x, self.controls.robot_min_y, self.robot_z)
            robot.move(self.controls.robot_max_x, self.controls.robot_max_y, self.robot_z)
            robot.move(self.controls.robot_min_x, self.controls.robot_max_y, self.robot_z)
            robot.move(self.controls.robot_min_x, self.controls.robot_min_y, self.robot_z)
            self.context.breakpoint()

    def test_two_finger(self):
        robot = self.context.robot
        robot_z = self.robot_z

        # Use low linear speed to avoid large angular speed.
        robot.set_speed(40, 40)

        center_x = (self.controls.robot_min_x + self.controls.robot_max_x) / 2
        center_y = (self.controls.robot_min_y + self.controls.robot_max_y) / 2

        self.context.html("Active finger: ")
        self.context.html(robot.get_active_finger())

        # Move robot to workspace center and do some rotations and finger separation changes.
        robot.move(center_x, center_y, robot_z)
        robot.move(center_x, center_y, robot_z, azimuth=-60)
        robot.set_finger_separation(50.0)
        robot.move(center_x, center_y, robot_z, azimuth=60)
        robot.set_finger_separation(30.0)
        robot.move(center_x, center_y, robot_z)
        robot.set_finger_separation(15.0)

        self.context.html("Finger Separation: ")
        self.context.html(robot.get_finger_separation())

        robot.set_active_finger(1)
        self.context.html("Active finger: ")
        self.context.html(robot.get_active_finger())

        robot.move(center_x, center_y, robot_z)
        robot.move(center_x, center_y, robot_z, azimuth=-60)
        robot.set_finger_separation(50.0)
        robot.move(center_x, center_y, robot_z, azimuth=60)
        robot.set_finger_separation(30.0)
        robot.move(center_x, center_y, robot_z)
        robot.set_finger_separation(15.0)

        robot.set_active_finger(0)
        self.context.html("Active finger: ")
        self.context.html(robot.get_active_finger())

    def test_tap(self):
        tap_count = 10

        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)
            for i in range(tap_count):
                # Update indicators in GUI.
                text = str(i + 1) + '/' + str(tap_count)
                self.context.indicators.set_test_detail("Tap", text)

                # Random point
                padding = 5
                rand_x = random.uniform(padding, dut.width - padding)
                rand_y = random.uniform(padding, dut.height - padding)
                clearance = random.uniform(-2, 3)

                # Random velocity
                if i == tap_count / 2:
                    velocity = random.uniform(1, 30)
                    acceleration = random.uniform(1, 100)
                    robot.set_speed(velocity, acceleration)
                velocity = random.uniform(velocity_min, velocity_max)
                acceleration = random.uniform(acceleration_min, acceleration_max)
                robot.set_speed(velocity, acceleration)

                dut.jump(rand_x, rand_y, base_distance, base_distance)

                # Perform tap gesture.
                dut.tap(rand_x, rand_y, clearance=clearance)
                dut.double_tap(rand_x, rand_y, clearance=clearance)

                # Check if test should pause or stop.
                self.context.breakpoint()
                break

    def test_swipe(self):
        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        for dut in self.duts:
            # Jump over DUT origin.
            base_distance = dut.base_distance
            dut.jump(0.0, 0.0, base_distance)
            swipe_count = 10

            for i in range(swipe_count):
                # Update indicators in GUI.
                text = str(i + 1) + '/' + str(swipe_count)
                self.context.indicators.set_test_detail("Swipe", text)

                # Random swipes
                padding = 5
                start_x = random.uniform(padding, dut.width - padding)
                start_y = random.uniform(padding, dut.height - padding)
                end_x = random.uniform(padding, dut.width - padding)
                end_y = random.uniform(padding, dut.height - padding)
                clearance = random.uniform(-2, 3)
                line = Containers.Line(start_x, start_y, clearance, end_x, end_y, clearance)

                # Set default velocity for jump
                self.context.set_robot_default_speed()

                # Jump with default velocity over the start point of the line to swipe.
                dut.jump(start_x, start_y, base_distance, base_distance)

                # Random velocity
                if i == swipe_count / 2:
                    velocity = random.uniform(1, 5)
                    acceleration = random.uniform(1, 10)
                    robot.set_speed(velocity, acceleration)
                velocity = random.uniform(velocity_min, velocity_max)
                acceleration = random.uniform(acceleration_min, acceleration_max)
                robot.set_speed(velocity, acceleration)

                # Perform swipe gesture.
                dut.swipe(line.start_x, line.start_y, line.end_x, line.end_y, clearance=clearance, radius=6)

                self.context.breakpoint()

    def test_random(self):
        random_count = 10

        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        for i in range(random_count):
            text = str(i + 1) + '/' + str(random_count)
            self.context.indicators.set_test_detail("Random gesture", text)

            test = random.randint(0, 3)
            dut_ix = random.randint(0, len(self.duts) - 1)
            dut = self.duts[dut_ix]

            base_distance = dut.base_distance

            padding = 5
            start_x = random.uniform(padding, dut.width - padding)
            start_y = random.uniform(padding, dut.height - padding)
            end_x = random.uniform(padding, dut.width - padding)
            end_y = random.uniform(padding, dut.height - padding)
            clearance = random.uniform(-2, 3)
            # Random velocity
            velocity = random.uniform(velocity_min, velocity_max)
            acceleration = random.uniform(acceleration_min, acceleration_max)
            robot.set_speed(velocity, acceleration)

            # Tap
            if test == 0:
                point = Containers.Point(start_x, start_y, 0)
                dut.jump(start_x, start_y, base_distance)
                dut.tap(start_x, start_y, clearance=clearance)
            # Swipe
            elif test == 1:
                dut.jump(start_x, start_y, base_distance)
                dut.swipe(start_x, start_y, end_x, end_y, clearance=clearance, radius=6)

            self.context.breakpoint()

    def test_pinch(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.pinch(x, y, 30.0, 60.0, 0.0)
            dut.pinch(x, y, 50.0, 40.0, 0.0)

            dut.pinch(x, y, 30.0, 60.0, -60.0)
            dut.pinch(x, y, 50.0, 40.0, -60.0)

            dut.pinch(x, y, 30.0, 60.0, 60.0)
            dut.pinch(x, y, 50.0, 40.0, 60.0)

            # Check if test should pause or stop.
            self.context.breakpoint()

    def test_drumroll(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.drumroll(x, y, 0.0, 50.0, 10, 5.0, -2.0)
            dut.drumroll(x, y, -60.0, 30.0, 15, 5.0, -2.0)
            dut.drumroll(x, y, 60.0, 70.0, 10, 3.0, -2.0)

    def test_compass(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.compass(x, y, -40.0, 40.0, 50.0, -2)
            dut.compass(x, y, -50.0, 50.0, 70.0, -2)

    def test_compass_tap(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.compass_tap(x, y, -40.0, 40.0, 45.0, 10.0, None, False, -2.0)
            dut.compass_tap(x, y, -60.0, 60.0, 75.0, 12.0, None, True, -2.0)

    def test_touch_and_tap(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.touch_and_tap(x, y, x + 40.0, y, None, 4, 1.0, 0.5, 0.1, 0)
            dut.touch_and_tap(x, y, x, y + 40.0, None, 4, 1.0, 0.5, 0.1, 0)

    def test_line_tap(self):
        for dut in self.duts:
            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)

            x = dut.width / 2
            y = dut.height / 2

            dut.line_tap(x, y, x + 60.0, y, [30.0, 35.0, 40.0], 30.0, 0.0, None, 0)
            dut.line_tap(x, y, x, y + 60.0, [30.0, 35.0, 40.0], 30.0, 0.0, None, 0)

    def test_press(self):
        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        press_count = 10
        for dut in self.duts:

            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)
            for i in range(press_count):
                # Update indicators in GUI.
                text = str(i + 1) + '/' + str(press_count)
                self.context.indicators.set_test_detail("Press", text)

                # Random point
                padding = 5
                rand_x = random.uniform(padding, dut.width - padding)
                rand_y = random.uniform(padding, dut.height - padding)
                clearance = random.uniform(-2, 3)

                # Random velocity
                if i == press_count / 2:
                    velocity = random.uniform(1, 30)
                    acceleration = random.uniform(1, 100)
                    robot.set_speed(velocity, acceleration)
                velocity = random.uniform(velocity_min, velocity_max)
                acceleration = random.uniform(acceleration_min, acceleration_max)
                robot.set_speed(velocity, acceleration)

                dut.jump(rand_x, rand_y, base_distance, base_distance)

                # Random force
                force = 100
                if i == press_count / 2:
                    force = random.uniform(1, 300)

                # Random duration
                duration = 0
                if i == press_count / 2:
                    duration = random.uniform(1, 5)

                # Perform tap gesture.
                dut.press(rand_x, rand_y, force, z=None, tilt=None, azimuth=None, duration=duration)

                # Check if test should pause or stop.
                self.context.breakpoint()

    def test_drag(self):
        robot = self.context.robot
        velocity_min = self.controls.min_velocity
        velocity_max = self.controls.max_velocity
        acceleration_min = self.controls.min_acceleration
        acceleration_max = self.controls.max_acceleration

        drag_count = 10
        for dut in self.duts:

            base_distance = dut.base_distance
            dut.jump(0, 0, base_distance)
            for i in range(drag_count):
                # Update indicators in GUI.
                text = str(i + 1) + '/' + str(drag_count)
                self.context.indicators.set_test_detail("Drag", text)

                # Random point
                padding = 5
                start_x = random.uniform(padding, dut.width - padding)
                start_y = random.uniform(padding, dut.height - padding)
                end_x = random.uniform(padding, dut.width - padding)
                end_y = random.uniform(padding, dut.height - padding)
                clearance = random.uniform(-2, 3)
                line = Containers.Line(start_x, start_y, clearance, end_x, end_y, clearance)

                # Random velocity
                if i == drag_count / 2:
                    velocity = random.uniform(1, 30)
                    acceleration = random.uniform(1, 100)
                    robot.set_speed(velocity, acceleration)
                velocity = random.uniform(velocity_min, velocity_max)
                acceleration = random.uniform(acceleration_min, acceleration_max)
                robot.set_speed(velocity, acceleration)

                dut.jump(start_x, start_y, base_distance, base_distance)

                # Random force
                force = 100
                if i == drag_count / 2:
                    force = random.uniform(1, 300)

                # Perform tap gesture.
                dut.drag(line.start_x, line.start_y, line.end_x, line.end_y, clearance=clearance)

                # TODO: drag_force is not working on server side yet
                # dut.drag_force(line.end_x, line.end_y, line.start_x, line.start_y, force)

                # Check if test should pause or stop.
                self.context.breakpoint()

    @staticmethod
    def get_best_score(results):
        score = 0

        for result in results:
            score = max(score, result['score'])

        return score

    def test_find_object(self):
        SHM_DIR = join_script_root_directory("background_images")

        for dut in self.duts:
            dut_node = self.context.duts_node.find_by_name(dut.name)
            self.context.duts_node.set_active_dut(dut_node)

            self.context.send_image(self.controls.object_image)

            for object_shm in self.controls.object_shm.split(","):
                object_shm = object_shm.strip()

                self.context.html("Object to Find: " + object_shm)

                response = dut.find_objects(path.join(SHM_DIR, object_shm))

                if not response["success"] or len(response["results"]) == 0:
                    raise Exception("Could not find object {}!".format(object_shm))

                for result in response["results"]:
                    self.context.html("Found object with result: " + str(result))

                if BurningTest.get_best_score(response["results"]) < 0.95:
                    raise Exception("Could not find object {} with sufficient score!".format(object_shm))

            self.context.send_image(None)

    def test_search_text(self):
        for dut in self.duts:
            dut_node = self.context.duts_node.find_by_name(dut.name)
            self.context.duts_node.set_active_dut(dut_node)

            self.context.send_image(self.controls.text_image)

            for text in self.controls.text_string.split(","):
                text = text.strip()

                self.context.html("Text to Find: " + text)

                response = dut.search_text(text)

                if not response["success"] or len(response["results"]) == 0:
                    raise Exception("Could not find text!")

                for result in response["results"]:
                    self.context.html("Found text with result: " + str(result))

                if BurningTest.get_best_score(response["results"]) < 0.95:
                    raise Exception("Could not find object {} with sufficient score!".format(object_shm))

            self.context.send_image(None)

    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        # Main sequence contains common resources and state.
        #tip = self.context.get_active_tip()
        robot = self.context.robot

        # Get selected duts from main sequence
        duts_node = self.context.duts_node
        self.duts = []

        if len(self.controls.dut1) > 0:
            dut = duts_node.find_by_name(self.controls.dut1)

            if dut is None:
                raise Exception("Could not find DUT " + self.controls.dut1)

            self.duts.append(dut.tnt_dut)

        if len(self.controls.dut2) > 0:
            dut = duts_node.find_by_name(self.controls.dut2)

            if dut is None:
                raise Exception("Could not find DUT " + self.controls.dut2)

            self.duts.append(dut.tnt_dut)

        if len(self.duts) == 0:
            raise Exception("Invalid dut(s)!")

        dut1 = self.duts[0] if len(self.duts) > 0 else None
        dut2 = self.duts[1] if len(self.duts) > 1 else None

        self.context.html("Running Burning Test for DUTs: {}, {}   tip: {}".format(dut1, dut2, "tip"))
        self.context.html("Testing time: {} hours".format(self.controls.testing_time))

        # For taking time
        done = False
        start_time = datetime.now()
        self.context.html("Starting time: {}".format(start_time))

        self.context.set_robot_default_speed()
        random.seed()

        # Set finger separation close to finger contact.
        #robot.set_finger_separation(15.0)

        # Run test until the desired time is full.
        while done is False:

            # Loop through all test definitions.
            for test_control_name in self.test_control_names:
                control = getattr(self.controls, test_control_name)

                # Check if test is enabled.
                if not control:
                    continue

                label = self.controls.info[test_control_name]["label"]

                self.context.html("Burning test: " + label)

                self.context.indicators.set_test_detail("Burning test", label)

                # Test method name is determined from control name.
                method_name = "test_" + test_control_name

                method = getattr(self, method_name)

                # Run test.
                method()

                self.context.breakpoint()

            # End of the burning test pattern. Home robot.
            self.context.breakpoint()
            robot.go_home()
            time_now = datetime.now()
            delta_time = time_now - start_time

            self.context.html("Testing time: {}".format(delta_time))

            if float(delta_time.seconds) / 60 / 60 >= float(self.controls.testing_time):
                done = True

        self.context.html("Burning test done.")

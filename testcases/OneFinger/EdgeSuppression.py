from TPPTcommon.Node import *
from TPPTcommon.grid import create_random_points,create_point_grid_custom, GridVisContainer,create_point_grid
from MeasurementDB import *
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
import time
logger = logging.getLogger(__name__)

# Database table name for the test case.
DB_TEST_TABLE_NAME = 'one_finger_edge_suppression_test'

# Database table name for the test results.
DB_RESULTS_TABLE_NAME = 'one_finger_edge_suppression_results'

# Database table indices associated with test case.
DB_TABLE_INDICES = [[DB_TEST_TABLE_NAME, 'test_id'], [DB_RESULTS_TABLE_NAME, 'point_id']]

class OneFingerEdgeSuppressionTest(Base):

    #One Finger Non Stationary Reporting Rate Test is defined here
    __tablename__ = DB_TEST_TABLE_NAME

    id = Column( Integer, primary_key = True )
    test_id = Column( Integer, ForeignKey('test_item.id', ondelete='CASCADE'), nullable=False )
    test = relation( TestItem, backref = backref(DB_TEST_TABLE_NAME, order_by = id) )

    #For straight lines, start and stop positions are defined
    robot_x = Column( Float )
    robot_y = Column( Float )


class OneFingerEdgeSuppressionResults(Base):

    #One Finger Non Stationary Reporting Rate results are defined here
    __tablename__ = DB_RESULTS_TABLE_NAME

    id = Column( Integer, primary_key = True )
    point_id = Column( Integer, ForeignKey(DB_TEST_TABLE_NAME + '.id', ondelete='CASCADE'), nullable=False )
    point = relation( OneFingerEdgeSuppressionTest, backref = backref(DB_RESULTS_TABLE_NAME, order_by = id) )

    panel_x = Column( Float )
    panel_y = Column( Float )
    sensitivity = Column( Float )
    delay = Column( Float )
    finger_id = Column( Integer )
    time = Column( Float )
    event = Column( Integer )

class EdgeSuppression(TestStep):
    """
    In stationary jitter test robot touches DUT surface for a moment while a stream of touch events is collected.
    In analysis it is inspected how much the reported locations vary.
    """

    def __init__(self, context):
        """
        The 'init' method mainly defines the test case controls for GUI and is executed when script is loaded.
        """

        super().__init__('Edge Suppression')

        self.context = context

        # ngocdd3

        actions = ['Tap','Long Tap']


        self.controls.action = actions[0]
        self.controls.info['action'] = {'label': 'Chooce Action', 'items': actions}

        self.controls.edgedistance = 6
        self.controls.info['edgedistance'] = {'label': 'Measurement Size [mm]', "min": 1}

        self.controls.delay = 2
        self.controls.info['delay'] = {'label': 'Interval time for each tap point [ms]', "min": 1}

        self.controls.heightsize = 10
        self.controls.info['heightsize'] = {'label': 'Height Size [mm]', "min": 1}

        self.controls.gridspacing = 1
        self.controls.info['girdspacing'] = {'label': 'GridSpacing [mm]', "min": 1}

        self.controls.margin = -0.1
        self.controls.info['margin'] = {'label': 'Margin [mm]', "min": -0.1}
        # ngocdd3


    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        dut = self.context.get_active_dut()
        tip = self.context.get_active_tip()

        base_distance = dut.base_distance

        self.context.html("Running One Finger Edge Suppression test for dut:%s, tip:%s"%(dut, tip))

        # Create database entry for the test case.
        test_item = self.context.create_db_test_item("One Finger Edge Suppression Test")

        measurement_points = self.create_grid(dut)

        # ngocdd3

        intervaltime = self.controls.delay

        for index, point in enumerate(measurement_points):
            # Update indicators in GUI.
            self.context.indicators.set_test_detail('Point', str(index + 1) + ' / ' +str(len(measurement_points)))

            # Create database entry for the point to be measured.
            point_id = self.create_point_id(point, test_item)

            # Move over the point.
            dut.move(point.x, point.y, base_distance)

            # Start continuous measurement to obtain a stream of touch events.
            # Timeout is used to stop event stream after robot has moved off the surface.
            continuous_measurement = self.context.create_continuous_measurement(None)
            continuous_measurement.point = point
            continuous_measurement.start(3.0)

            if(self.controls.action=='Long Tap'):
                # Move to touch the DUT surface.
                dut.move(point.x, point.y, -1.0)

                # Store touchlist length so that we know the event from the moment the move gesture ended.
                touchlist_len = len(continuous_measurement.parse_data())

                # Sleep for a while to get a good stream of touch values.
                time.sleep(intervaltime)

                # Get measurement data before moving off DUT to avoid getting garbage data from lift-off.
                touchlist = continuous_measurement.parse_data()

                # Move off DUT surface to end jitter data collection.
                dut.move(point.x, point.y, base_distance)
            else:
                # Move to touch the DUT surface.
                # dut.tap(point.x, point.y, -1.0)
                dut.tap(point.x, point.y, z=None, tilt=None, azimuth=None, clearance=-1.0, duration=None)

                # Store touchlist length so that we know the event from the moment the move gesture ended.
                touchlist_len = len(continuous_measurement.parse_data())

                # Get measurement data before moving off DUT to avoid getting garbage data from lift-off.
                touchlist = continuous_measurement.parse_data()

                # Move off DUT surface to end jitter data collection.
                dut.move(point.x, point.y, base_distance)


            # End measurement.
            continuous_measurement.end()

            # Get touch events. touchlist_len divides the events into two sets:
            # 1) Events from the duration of move gesture and 2) events during contact with no robot movement.
            # Take the last event from set 1 and all events from set 2.
            # This is because some devices have practically no jitter so that set 2 is most often empty.
            # We want to have at least one event in touchlist to indicate that there was no error in the measurement.
            if touchlist_len > 0:
                touchlist = touchlist[touchlist_len - 1:]

            # Save results to database.
            self.save_measurement_data(point_id, touchlist)

            # Check if test should pause or stop.
            self.context.breakpoint()

        self.context.close_db_test_item(test_item)

# ngocdd3
    def create_grid(self, dut):
        """
        Create grid of points that define the geometry of the test case.
        :param dut: DUT where the grid is evaluated on.
        """
        grid = float(self.controls.gridspacing)
        heightsize = float(self.controls.heightsize)
        edge = float(self.controls.edgedistance)
        margin = float(self.controls.margin)
        return create_point_grid_custom(dut, grid, margin,edge,heightsize)

    def visualize_grid(self, dut):
        """
        Construct a visualization of the test case grid.
        :param dut: DUT where the grid is evaluated on.
        """

        return GridVisContainer(self.__class__.__name__, (dut.width, dut.height), self.create_grid(dut), dut.name)

# ngocdd3

    def create_point_id(self,point, test_item):

        stationary_jitter_test = OneFingerEdgeSuppressionTest()

        stationary_jitter_test.robot_x = point.x
        stationary_jitter_test.robot_y = point.y
        stationary_jitter_test.test_id = test_item.id
        self.context.db.add(stationary_jitter_test)

        return stationary_jitter_test.id

    def save_measurement_data(self, point_id, touchlist):
        """
        Save stationary jitter measurement to database.
        """
        dblist = []

        for testresult in touchlist:
            test_results_stationary_jitter = OneFingerEdgeSuppressionResults()

            test_results_stationary_jitter.panel_x = float(testresult[0])
            test_results_stationary_jitter.panel_y = float(testresult[1])
            test_results_stationary_jitter.sensitivity = float(testresult[2])
            test_results_stationary_jitter.finger_id = int(testresult[3])
            test_results_stationary_jitter.delay = testresult[4]
            test_results_stationary_jitter.time = testresult[5]
            test_results_stationary_jitter.event = testresult[6]
            test_results_stationary_jitter.point_id = point_id
            dblist.append(test_results_stationary_jitter)

            self.context.add_dut_point(float(testresult[0]), float(testresult[1]))

        self.context.db.addAll(dblist)
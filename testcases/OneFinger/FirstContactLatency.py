from TPPTcommon.Node import *
from TPPTcommon.grid import create_random_points, GridVisContainer
import numpy
from MeasurementDB import *
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
logger = logging.getLogger(__name__)

# Database table name for the test case.
DB_TABLE_NAME = 'one_finger_first_contact_latency_test'

# Database table indices associated with test case.
DB_TABLE_INDICES = [[DB_TABLE_NAME, 'test_id']]

class OneFingerFirstContactLatencyTest(Base):

    #One-finger latency test is defined here

    __tablename__ = DB_TABLE_NAME

    id = Column(Integer, primary_key=True)
    test_id = Column( Integer, ForeignKey('test_item.id', ondelete='CASCADE'), nullable=False )
    test = relation( TestItem, backref=backref(DB_TABLE_NAME, order_by=id) )

    #One-finger Latency test results
    powerstate = Column( Integer )
    system_latency = Column( Float )
    delay = Column( Float )
    time = Column( Float )
    robot_x = Column( Float )
    robot_y = Column( Float )
    robot_z = Column( Float )

class FirstContactLatency(TestStep):
    """
    First contact latency taps DUT surface and measures latency from first physical contact
    to the reported touch event. Uses PIT to get accurate touch timing.
    """

    def __init__(self, context):
        """
        The 'init' method mainly defines the test case controls for GUI and is executed when script is loaded.
        """

        super().__init__('First Contact Latency')

        self.context = context

        self.controls.AmountOfPoints = 5
        self.controls.info['AmountOfPoints'] = {'label': 'Amount of points per test', "min": 1}

        self.controls.calibrateSystemLatency = False
        self.controls.info['calibrateSystemLatency'] = {'label': 'Calibrate system latency'}

    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        dut = self.context.get_active_dut()
        tip = self.context.get_active_tip()

        base_distance = dut.base_distance

        # PIT is required for this test case.
        if self.context.get_active_dut_driver() != "PIT":
            self.context.html_color("One Finger First Contact Latency Test can only be executed with PIT", "red")
            return

        pit = self.context.pit
        system_latency = 0.0
        power_states = {"Active":1, "Idle":2}

        self.context.html("Running One First Contact Latency Test for dut:%s, tip:%s"%(dut, tip))

        test_item = self.context.create_db_test_item("One Finger First Contact Latency Test")
        point_number = 1
        measurement_points = self.create_grid(dut)

        if self.controls.calibrateSystemLatency:
            """System delay needs to be calibrated because otherwise delay values are too big"""
            self.calibrationtarget = self.context.tnt.dut("OF")
            self.calibrationtarget.jump(0.2, 0.2,base_distance)
            self.calibrationtarget.move(0.0,0.0)
            system_latencies = []
            #pit.NormalTrigger()

            for tap_count in range(10):
                pit.SingleTrigger()
                self.calibrationtarget.tap(0.2, 0.2)
                timestamps = pit.ReturnTimestamps()
                self.context.html(timestamps)
                system_latencies.append((timestamps[1]-timestamps[2]))
                system_latency = round(numpy.median(system_latencies),2)
                self.context.breakpoint()

            self.context.html("System latency median: " + str(system_latency) + " ms")
            pit.NormalTrigger()

        dut.jump(0.0,0.0,base_distance)
        dut.move(0.0,0.0)

        for point in measurement_points:
            for power_state in power_states:
                dut.jump(point.x, point.y, base_distance, base_distance)
                pit.WriteSleepMode(power_states[power_state])
                pit.SingleTrigger()

                """Tap each measurement point and store results to database"""
                self.context.indicators.set_test_details([('Power state', power_state), ('Point', str(point_number) + ' / ' + str(len(measurement_points)))])

                tap_measurement = self.context.create_tap_measurement(point)
                tap_measurement.start()
                dut.tap(point.x,point.y)
                tap_measurement.end()

                timestamps = pit.ReturnTimestamps()
                self.save_measurement_data(tap_measurement.results, point, test_item, power_states[power_state], system_latency, timestamps)

                # Check if test should pause or stop.
                self.context.breakpoint()

            point_number += 1

        pit.NormalTrigger()
        pit.WriteSleepMode(power_states["Active"])
        self.context.close_db_test_item(test_item)

    def create_grid(self, dut):
        """
        Create grid of points that define the geometry of the test case.
        :param dut: DUT where the grid is evaluated on.
        """

        return create_random_points(dut, self.controls.AmountOfPoints)

    def visualize_grid(self, dut):
        """
        Construct a visualization of the test case grid.
        :param dut: DUT where the grid is evaluated on.
        """

        return GridVisContainer(self.__class__.__name__, (dut.width, dut.height), self.create_grid(dut), dut.name)

    def save_measurement_data(self, measured_point_data, robot_point, tap_test_item, power_state, system_latency, timestamps):
        """
        Save first contact latency measurement to database.
        """

        test_result = OneFingerFirstContactLatencyTest()

        test_result.test_id = tap_test_item.id
        test_result.robot_x = robot_point.x
        test_result.robot_y = robot_point.y
        test_result.robot_z = 0.0#meas_point[2]
        test_result.powerstate = power_state
        test_result.system_latency = system_latency

        if len(measured_point_data) > 0:
            p2p_data = measured_point_data[0]
            test_result.delay = timestamps[1]
            test_result.time = timestamps[0] #p2p_data[5]
            test_result.panel_x = p2p_data[0]
            test_result.panel_y = p2p_data[1]

        self.context.db.add(test_result)
from TPPTcommon.Node import *
from TPPTcommon.grid import create_random_points, GridVisContainer,create_point_grid
from MeasurementDB import *
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
logger = logging.getLogger(__name__)

# Database table name for the test case.
DB_TABLE_NAME = 'one_finger_tap_precision_test'

# Database table indices associated with test case.
DB_TABLE_INDICES = [[DB_TABLE_NAME, 'test_id']]

class OneFingerTapPrecisionTest( Base ):

    #One-finger tap results are defined here
    __tablename__ = DB_TABLE_NAME

    id = Column(Integer, primary_key=True)
    test_id = Column( Integer, ForeignKey('test_item.id', ondelete='CASCADE'), nullable=False )
    test = relation( TestItem, backref=backref(DB_TABLE_NAME, order_by=id) )

    #Common parameters
    robot_x = Column( Float )
    robot_y = Column( Float )
    robot_z = Column( Float )
    point_number = Column( Integer )

    #Results
    panel_x = Column( Float )
    panel_y = Column( Float )
    sensitivity = Column( Float )
    finger_id = Column( Integer )
    delay = Column( Float )
    time = Column( Float )
    event = Column( Integer )

class Precision(TestStep):
    """
    In repeatability test DUT is tapped at random locations multiple times per location.
    Touch events are used in analysis to inspect how close-by taps at single location are.
    """

    def __init__(self, context):
        """
        The 'init' method mainly defines the test case controls for GUI and is executed when script is loaded.
        """

        super().__init__('Precision')

        self.context = context

        self.controls.AmountOfTaps = 5
        self.controls.info['AmountOfTaps'] = {'label': 'Amount of taps per point', "min": 2}
        #
        # self.controls.AmountOfPoints = 5
        # self.controls.info['AmountOfPoints'] = {'label': 'Amount of points per test', "min": 1}

        self.controls.delaytime = 0
        self.controls.info['delaytime'] = {'label': 'Delay time of points per test', "min": 0}

    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        dut = self.context.get_active_dut()
        tip = self.context.get_active_tip()

        base_distance = dut.base_distance

        # Measurement timeout for tap. This should have some margin.
        tap_timeout = 2 * self.context.get_travel_time(base_distance) + 2.0

        self.context.html("Running One Finger Precision test for dut:%s, tip:%s"%(dut, tip))

        # Create database item for the test.
        test_item = self.context.create_db_test_item("One Finger Precision Test")

        measurement_points = self.create_grid(dut)
        interval_time = self.context.settings_node.controls.default_duration

        # ngocdd3
        delaytime = float(self.controls.delaytime)
        for point_index, point in enumerate(measurement_points):
            # Move over tap location. This might be far away from current location.
            # This makes sure that measurement timeout appropriately describes the tap gesture.
            dut.move(point.x, point.y, base_distance)

            for tap in range(self.controls.AmountOfTaps):
                point_number = point_index + 1

                # Update test indicators in GUI.
                tap_info = str(tap + 1) + ' / ' + str(self.controls.AmountOfTaps)
                point_info = str(point_number) + ' / ' + str(len(measurement_points))
                self.context.indicators.set_test_details([('Tap', tap_info), ('Point', point_info)])

                # Start measuring tap event.
                tap_measurement = self.context.create_tap_measurement(point)
                tap_measurement.start(timeout=tap_timeout)

                # Tap DUT at current test location.
                dut.tap(point.x, point.y, clearance=-1.0, duration=interval_time)
                # dut.press(point.x, point.y,400, z=None, tilt=None, azimuth=None, duration=2, press_depth=-1)
                # dut.tap(point.x,point.y, clearance=-1.0)

                # End measuring tap event.
                tap_measurement.end()

                # Save tap measurement to database.
                self.save_measurement_data(tap_measurement.results, point, test_item, point_number)

                # ngocdd3
                # delay time for each tap action
                time.sleep(delaytime)

                # Check if test should pause or stop.
                self.context.breakpoint()

        self.context.close_db_test_item(test_item)

    def create_grid(self, dut):
        """
        Create grid of points that define the geometry of the test case.
        :param dut: DUT where the grid is evaluated on.
        """
        grid = float(self.context.settings_node.controls.grid_spacing)
        margin = float(self.context.settings_node.controls.default_margin)
        return create_point_grid(dut, grid, margin)
        # return create_random_points(dut, self.controls.AmountOfPoints)

    def visualize_grid(self, dut):
        """
        Construct a visualization of the test case grid.
        :param dut: DUT where the grid is evaluated on.
        """

        return GridVisContainer(self.__class__.__name__, (dut.width, dut.height), self.create_grid(dut), dut.name)

    def save_measurement_data(self, measured_point_data, robot_point, tap_test_item, index):
        """
        Save repeatability measurement to database.
        """
        test_result = OneFingerTapPrecisionTest()

        test_result.test_id = tap_test_item.id
        test_result.robot_x = robot_point.x
        test_result.robot_y = robot_point.y
        test_result.robot_z = 0.0 #meas_point[2]
        test_result.point_number = int(index)

        if len(measured_point_data) > 0:
            p2p_data = measured_point_data[0]
            test_result.panel_x = p2p_data[0]
            test_result.panel_y = p2p_data[1]
            test_result.sensitivity = p2p_data[2]
            test_result.finger_id = p2p_data[3]

            self.context.add_dut_point(p2p_data[0], p2p_data[1])

        self.context.db.add(test_result)
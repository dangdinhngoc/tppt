from TPPTcommon.Node import *
from TPPTcommon.grid import create_vertical_horizontal_line_grid_custom, create_diagonal_line_grid_custom, \
    create_worst_case_lines_custom, GridVisContainer
from MeasurementDB import *
from TPPTcommon.Util import calculate_timeout
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
logger = logging.getLogger(__name__)

# Database table name for the test case.
DB_TEST_TABLE_NAME = 'one_finger_swipe_test'

# Database table name for the test results.
DB_RESULTS_TABLE_NAME = 'one_finger_swipe_results'

# Database table indices associated with test case.
DB_TABLE_INDICES = [[DB_TEST_TABLE_NAME, 'test_id'], [DB_RESULTS_TABLE_NAME, 'swipe_id']]

class OneFingerSwipeTest( Base ):

    #One-finger swipe test is defined here
    __tablename__ = DB_TEST_TABLE_NAME

    id = Column( Integer, primary_key = True )
    test_id = Column( Integer, ForeignKey('test_item.id', ondelete='CASCADE'), nullable=False )
    test = relation( TestItem, backref = backref(DB_TEST_TABLE_NAME, order_by = id) )

    #For straight lines, start and stop positions are defined
    start_x = Column( Float )
    start_y = Column( Float )
    end_x = Column( Float )
    end_y = Column( Float )

    #For circular lines, also through position and radius in x and y directions are defined
    through_x = Column( Float )
    through_y = Column( Float )
    radius_x = Column( Float )
    radius_y = Column( Float )

class OneFingerSwipeResults( Base ):

    #One-finger swipe results are defined here
    __tablename__ = DB_RESULTS_TABLE_NAME

    id = Column( Integer, primary_key = True )
    swipe_id = Column( Integer, ForeignKey(DB_TEST_TABLE_NAME + '.id', ondelete='CASCADE'), nullable=False )
    swipe = relation( OneFingerSwipeTest, backref = backref(DB_RESULTS_TABLE_NAME, order_by = id) )

    panel_x = Column( Float )
    panel_y = Column( Float )
    sensitivity = Column( Float )
    delay = Column( Float )
    finger_id = Column( Integer )
    time = Column( Float )
    event = Column( Integer )

class Swipe(TestStep):
    """
    In swipe test DUT is swept along set of lines and touch events are collected as continuous stream.
    """

    def __init__(self, context):
        super().__init__('Swipe')
        """
        The 'init' method mainly defines the test case controls for GUI and is executed when script is loaded.
        """

        self.context = context

        self.controls.edge = False
        self.controls.info['edge'] = {'label': 'Edge area'}

        self.controls.center = False
        self.controls.info['center'] = {'label': 'Center area'}


        self.controls.worstcases = True
        self.controls.info['worstcases'] = {'label': 'Worstcases'}

        self.controls.verticallines = False
        self.controls.info['verticallines'] = {'label': 'Vertical/Horizontal lines'}

        self.controls.diagonallines = False
        self.controls.info['diagonallines'] = {'label': 'Diagonal lines'}

        # ngocdd3
        self.controls.girdspacing = 10
        self.controls.info['girdspacing'] = {'label': 'Gird Spacing'}

        self.controls.topedge = 0
        self.controls.info['topedge'] = {'label': 'Top Edge'}

        # self.controls.startoffset = 0.0
        # self.controls.info['startoffset'] = {'label': 'Start offset [mm]', "validate":('(^[+]?\d+(?:\.\d+)?(?:[eE][+]\d+)?$)', "Correct format is 'x mm'(eg. 2 or 2.0)")}

    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        dut = self.context.get_active_dut()
        tip = self.context.get_active_tip()

        speed = self.context.settings_node.controls.line_drawing_speed
        finger_depth = self.context.settings_node.controls.finger_depth
        base_distance = dut.base_distance

        # Swipe test consists of multiple optional sets of swipes.
        tests = []
        if self.controls.verticallines:
            tests.append("vertical_horizontal")
        if self.controls.diagonallines:
            tests.append("diagonal")
        if self.controls.edge:
            tests.append("edge")
        if self.controls.center:
            tests.append("center")
        if self.controls.worstcases:
            tests.append("worstcases")

            # Measurement timeout for tap. This should have some margin.
            tap_timeout = 2 * self.context.get_travel_time(base_distance) + 2.0

        for test in tests:
            self.context.html("Running One Finger Swipe test for dut:%s, tip:%s. Test pattern is %s" % (dut, tip, test))

            # Create database entry for the test case. Each swipe set is separate test case.
            test_item = self.context.create_db_test_item("One Finger Swipe Test")

            # Create swipe lines for current test set.
            measurement_lines = self.create_grid(dut, test)

            for index, line in enumerate(measurement_lines):
                # Update indicators in GUI.
                self.context.indicators.set_test_detail('Line', str(index + 1) + ' / ' +str(len(measurement_lines)))

                # Create database entry for the line to swipe.
                swipe_id = self.create_line_id(line, test_item)

                # Jump with default speed over the start point of the line to swipe.
                self.context.set_robot_default_speed()
                dut.jump(line.start_x, line.start_y, base_distance, base_distance)

                # Swipe with specific speed.
                self.context.set_robot_speed(speed)

                # Start capturing a continuous stream of touch events.
                continuous_measurement = self.context.create_continuous_measurement(line)
                continuous_measurement.start(tap_timeout)

                # Perform swipe gesture.
                dut.swipe(line.start_x, line.start_y, line.end_x, line.end_y, clearance=finger_depth, radius=20)
                # dut.def drag( x1, y1, x2, y2, z=None, tilt1=None, tilt2=None, azimuth1=None, azimuth2=None,clearance=0,predelay=0, postdelay=0)

                # End measuring touch events.
                continuous_measurement.end()

                # Parse touch event data.
                touchlist = continuous_measurement.parse_data()

                # Save results to database.
                self.save_measurement_data(swipe_id, touchlist)

                # Check if test should pause or stop.
                self.context.breakpoint()

            self.item = self.context.close_db_test_item(test_item)

    def create_grid(self, dut, pattern):
        """
        Create grid of lines that define the geometry of the test case.
        :param dut: DUT where the grid is evaluated on.
        :param pattern: Pattern string that defines sweep line set.
        :param topedge: Set offset for  waterproof screen.
        """
        topedge = float(self.controls.topedge)
        grid_spacing = float(self.controls.girdspacing)
        margin = float(self.context.settings_node.controls.default_margin)

        if pattern == "vertical_horizontal":
            return create_vertical_horizontal_line_grid_custom(dut, grid_spacing,margin)
        elif pattern == "diagonal":
            return create_diagonal_line_grid_custom(dut, grid_spacing,margin)
        elif pattern == "edge":
            return create_worst_case_lines_custom(dut,margin,pattern)
        elif pattern == "center":
            return create_worst_case_lines_custom(dut, margin,pattern)
        elif pattern == "worstcases":
            return create_worst_case_lines_custom(dut, margin, pattern,topedge)
        else:
            assert False
        
    def visualize_grid(self, dut):
        """
        Construct a visualization of the test case grid.
        :param dut: DUT where the grid is evaluated on.
        """

        test_pattern = []

        # Show swipe lines of all enabled sets.
        if self.controls.verticallines:
            test_pattern += self.create_grid(dut, "vertical_horizontal")
        if self.controls.diagonallines:
            test_pattern += self.create_grid(dut, "diagonal")
        if self.controls.edge:
            test_pattern += self.create_grid(dut, "edge")
        if self.controls.center:
            test_pattern += self.create_grid(dut, "center")
        if self.controls.worstcases:
            test_pattern += self.create_grid(dut, "worstcases")


        return GridVisContainer(self.__class__.__name__, (dut.width, dut.height), test_pattern, dut.name)

    def create_line_id(self, line, test_item):

        swipetest = OneFingerSwipeTest()

        swipetest.start_x = line.start_x
        swipetest.start_y = line.start_y
        swipetest.end_x = line.end_x
        swipetest.end_y = line.end_y
        swipetest.test_id = test_item.id
        self.context.db.add(swipetest)

        return swipetest.id

    def save_measurement_data(self, line_id, touchlist):
        """
        Save swipe measurement to database.
        """

        dblist = []

        for testresult in touchlist:
            testresultswipe = OneFingerSwipeResults()

            testresultswipe.panel_x = float(testresult[0])
            testresultswipe.panel_y = float(testresult[1])
            testresultswipe.sensitivity = float(testresult[2])
            testresultswipe.finger_id = int(testresult[3])
            testresultswipe.delay = testresult[4]
            testresultswipe.time = testresult[5]
            testresultswipe.event = testresult[6]
            testresultswipe.swipe_id = line_id
            dblist.append(testresultswipe)

            self.context.add_dut_point(float(testresult[0]), float(testresult[1]))

        self.context.db.addAll(dblist)
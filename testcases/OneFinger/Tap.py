from TPPTcommon.Node import *
from TPPTcommon.grid import create_point_grid, create_grid_from_file, GridVisContainer
from MeasurementDB import *
# Logging module is needed if script wants to send logging
# information to the debug window.
import logging
import time
import os
logger = logging.getLogger(__name__)

# Database table name for the test case.
DB_TABLE_NAME = 'one_finger_tap_test'

# Database table indices associated with test case.
DB_TABLE_INDICES = [[DB_TABLE_NAME, 'test_id']]

class OneFingerTapTest( Base ):

    #One-finger tap results are defined here
    __tablename__ = DB_TABLE_NAME

    id = Column(Integer, primary_key=True)
    test_id = Column( Integer, ForeignKey('test_item.id', ondelete='CASCADE'), nullable=False )
    test = relation( TestItem, backref=backref(DB_TABLE_NAME, order_by=id) )

    #Are results single tap or jitter
    jitter = Column( Boolean )

    #Common parameters
    robot_x = Column( Float )
    robot_y = Column( Float )
    robot_z = Column( Float )
    point_number = Column( Integer )

    #Results
    panel_x = Column( Float )
    panel_y = Column( Float )
    sensitivity = Column( Float )
    finger_id = Column( Integer )
    delay = Column( Float )
    time = Column( Float )

class Tap(TestStep):
    """
    In tap test robot taps DUT surface at a regular grid of points and touch event for each tap is recorded.
    User can also supply a custom set of points as a "grid file".
    """

    def __init__(self, context):
        super().__init__('Tap')

        self.context = context

        # Directory that contains the user grid files.
        # self.grid_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), ".." , "TPPTcommon", "GridFiles")
        #
        # self.controls.usegridfile = False
        # self.controls.info['usegridfile'] = {'label': 'Use grid file'}
        #
        # try:
        #     gridfiles = os.listdir(self.grid_dir)
        #     self.controls.gridfile = gridfiles[0]
        # except FileNotFoundError:
        #     gridfiles = []
        #     self.controls.gridfile = ""

        self.controls.AmountOfTaps = 1
        self.controls.info['AmountOfTaps'] = {'label': 'Amount of taps per point', "min": 1}

        # self.controls.info['gridfile'] = {'label': 'Grid file', 'items': gridfiles}
        # self.controls.gridunit = "mm"
        # self.controls.info['gridunit'] = {'label': 'Grid unit', 'items': {'mm', '%'}}

    def execute(self):
        """
        The 'execute' method contains the actual work that the
        test step is doing.
        """

        # Main sequence contains common resources and state.
        dut = self.context.get_active_dut()
        tip = self.context.get_active_tip()

        base_distance = dut.base_distance

        # Measurement timeout for tap. This should have some margin.
        tap_timeout = 2 * self.context.get_travel_time(base_distance) + 8.0

        self.context.html("Running One Finger Tap test for DUT: %s, tip: %s" % (dut, tip))

        # Create database entry for the test case.
        tap_test_item = self.context.create_db_test_item("One Finger Tap Test")

        # Get grid of points to tap.
        measurement_points = self.create_grid(dut)

        interval_time = self.context.settings_node.controls.default_duration

        for index, point in enumerate(measurement_points):
            # Move over tap location. This might be far away from current location.
            # This makes sure that measurement timeout appropriately describes the tap gesture.
            dut.move(point.x, point.y, base_distance)

            for tap in range(self.controls.AmountOfTaps):
                point_number = index + 1

                # Update indicators in GUI.
                tap_info = str(tap + 1) + ' / ' + str(self.controls.AmountOfTaps)
                point_info = str(point_number) + ' / ' + str(len(measurement_points))
                self.context.indicators.set_test_details([('Tap', tap_info), ('Point', point_info)])
                # self.context.indicators.set_test_detail('Point', str(point_number) + ' / ' + str(len(measurement_points)))

                # Start tap measurement.
                tap_measurement = self.context.create_tap_measurement(point)
                tap_measurement.start(tap_timeout)

                # Perform tap gesture.
                dut.tap(point.x, point.y, clearance=-1.0,duration=interval_time)
                # dut.press(point.x, point.y,800, z=None, tilt=None, azimuth=None, duration=2, press_depth=-1)
                # dut.drag_force(29.87, 83.56, 29.53, 27.52, 900, z=None, tilt1=None, tilt2=None, azimuth1=None, azimuth2=None)
                # dut.double_tap(dut.height/2, dut.width/2, z=None, tilt=None, azimuth=None, clearance=0, duration=None, interval=0)
                # dut.pinch(dut.width/2, dut.height/2, 15, 11, 0, z=None, clearance=-1)
                # End tap measurement.
                tap_measurement.end()

                # Save results to database.
                self.save_measurement_data(tap_measurement.results, point, tap_test_item, point_number)

                # Check if test should pause or stop.
                self.context.breakpoint()

        self.context.close_db_test_item(tap_test_item)

# ngocdd3
    def create_grid(self, dut):
        """ the grid is evaluated on.
        Create grid of points that define the geometry of the test case.
        :param dut: DUT where
        """
        grid = float(self.context.settings_node.controls.grid_spacing)
        margin = float(self.context.settings_node.controls.default_margin)
        return create_point_grid(dut, grid, margin)

    def visualize_grid(self, dut):
        """
        Construct a visualization of the test case grid.
        :param dut: DUT where the grid is evaluated on.
        """
        return GridVisContainer(self.__class__.__name__, (dut.width, dut.height), self.create_grid(dut), dut.name)

# ngocdd3
    def save_measurement_data(self, measured_point_data, robot_point, tap_test_item, index):
        """
        Save tap test measurement to database.
        """

        test_result = OneFingerTapTest()
        test_result.jitter = False
        test_result.test_id = tap_test_item.id
        test_result.robot_x = robot_point.x
        test_result.robot_y = robot_point.y
        test_result.robot_z = 0.0 #meas_point[2]
        test_result.point_number = int(index)

        if len(measured_point_data) > 0:
            if measured_point_data[-2] == "OK":
                p2p_data = measured_point_data[0]
                test_result.panel_x = p2p_data[0]
                test_result.panel_y = p2p_data[1]
                test_result.sensitivity = p2p_data[2]
                test_result.finger_id = p2p_data[3]
                self.context.add_dut_point(p2p_data[0], p2p_data[1])

        self.context.db.add(test_result)